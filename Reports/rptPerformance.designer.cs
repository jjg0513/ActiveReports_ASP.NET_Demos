namespace ActiveReportsDemo.Reports
{
    /// <summary>
    /// Summary description for rptPerformance.
    /// </summary>
    partial class rptPerformance
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(rptPerformance));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.shape1,
            this.label1,
            this.label2,
            this.label3,
            this.label4,
            this.label5,
            this.label6,
            this.label7});
            this.pageHeader.Height = 0.898F;
            this.pageHeader.Name = "pageHeader";
            // 
            // label1
            // 
            this.label1.Height = 0.698F;
            this.label1.HyperLink = null;
            this.label1.Left = 0F;
            this.label1.Name = "label1";
            this.label1.Style = "font-family: Microsoft YaHei; font-size: 16pt; font-weight: bold; text-align: cen" +
    "ter; vertical-align: middle";
            this.label1.Text = "ActiveReports 性能验证";
            this.label1.Top = 0F;
            this.label1.Width = 6F;
            // 
            // label2
            // 
            this.label2.Height = 0.2F;
            this.label2.HyperLink = null;
            this.label2.Left = 0F;
            this.label2.Name = "label2";
            this.label2.Padding = new GrapeCity.ActiveReports.PaddingEx(2, 0, 0, 0);
            this.label2.Style = "color: White; font-family: Microsoft YaHei; font-size: 10pt; font-weight: bold; t" +
    "ext-align: center";
            this.label2.Text = "第一列";
            this.label2.Top = 0.698F;
            this.label2.Width = 1F;
            // 
            // label3
            // 
            this.label3.Height = 0.2F;
            this.label3.HyperLink = null;
            this.label3.Left = 1F;
            this.label3.Name = "label3";
            this.label3.Padding = new GrapeCity.ActiveReports.PaddingEx(2, 0, 0, 0);
            this.label3.Style = "color: White; font-family: Microsoft YaHei; font-size: 10pt; font-weight: bold; t" +
    "ext-align: center";
            this.label3.Text = "第二列";
            this.label3.Top = 0.698F;
            this.label3.Width = 1F;
            // 
            // label4
            // 
            this.label4.Height = 0.2F;
            this.label4.HyperLink = null;
            this.label4.Left = 2F;
            this.label4.Name = "label4";
            this.label4.Padding = new GrapeCity.ActiveReports.PaddingEx(2, 0, 0, 0);
            this.label4.Style = "color: White; font-family: Microsoft YaHei; font-size: 10pt; font-weight: bold; t" +
    "ext-align: center";
            this.label4.Text = "第三列";
            this.label4.Top = 0.698F;
            this.label4.Width = 1F;
            // 
            // label5
            // 
            this.label5.Height = 0.2F;
            this.label5.HyperLink = null;
            this.label5.Left = 3F;
            this.label5.Name = "label5";
            this.label5.Padding = new GrapeCity.ActiveReports.PaddingEx(2, 0, 0, 0);
            this.label5.Style = "color: White; font-family: Microsoft YaHei; font-size: 10pt; font-weight: bold; t" +
    "ext-align: center";
            this.label5.Text = "第四列";
            this.label5.Top = 0.698F;
            this.label5.Width = 1F;
            // 
            // label6
            // 
            this.label6.Height = 0.2F;
            this.label6.HyperLink = null;
            this.label6.Left = 4F;
            this.label6.Name = "label6";
            this.label6.Padding = new GrapeCity.ActiveReports.PaddingEx(2, 0, 0, 0);
            this.label6.Style = "color: White; font-family: Microsoft YaHei; font-size: 10pt; font-weight: bold; t" +
    "ext-align: center";
            this.label6.Text = "第五列";
            this.label6.Top = 0.698F;
            this.label6.Width = 1F;
            // 
            // label7
            // 
            this.label7.Height = 0.2F;
            this.label7.HyperLink = null;
            this.label7.Left = 5F;
            this.label7.Name = "label7";
            this.label7.Padding = new GrapeCity.ActiveReports.PaddingEx(2, 0, 0, 0);
            this.label7.Style = "color: White; font-family: Microsoft YaHei; font-size: 10pt; font-weight: bold; t" +
    "ext-align: center";
            this.label7.Text = "第六列";
            this.label7.Top = 0.698F;
            this.label7.Width = 1F;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox1,
            this.textBox2,
            this.textBox3,
            this.textBox4,
            this.textBox5,
            this.textBox6,
            this.line1});
            this.detail.Height = 0.2F;
            this.detail.Name = "detail";
            // 
            // textBox1
            // 
            this.textBox1.CanGrow = false;
            this.textBox1.DataField = "Col1";
            this.textBox1.Height = 0.2F;
            this.textBox1.Left = 0F;
            this.textBox1.Name = "textBox1";
            this.textBox1.Padding = new GrapeCity.ActiveReports.PaddingEx(2, 0, 0, 0);
            this.textBox1.Style = "font-family: Microsoft YaHei";
            this.textBox1.Text = "textBox1";
            this.textBox1.Top = 0F;
            this.textBox1.Width = 1F;
            // 
            // textBox2
            // 
            this.textBox2.CanGrow = false;
            this.textBox2.DataField = "Col1";
            this.textBox2.Height = 0.2F;
            this.textBox2.Left = 1F;
            this.textBox2.Name = "textBox2";
            this.textBox2.Padding = new GrapeCity.ActiveReports.PaddingEx(2, 0, 0, 0);
            this.textBox2.Style = "font-family: Microsoft YaHei";
            this.textBox2.Text = "textBox2";
            this.textBox2.Top = 0F;
            this.textBox2.Width = 1F;
            // 
            // textBox3
            // 
            this.textBox3.CanGrow = false;
            this.textBox3.DataField = "Col1";
            this.textBox3.Height = 0.2F;
            this.textBox3.Left = 2F;
            this.textBox3.Name = "textBox3";
            this.textBox3.Padding = new GrapeCity.ActiveReports.PaddingEx(2, 0, 0, 0);
            this.textBox3.Style = "font-family: Microsoft YaHei";
            this.textBox3.Text = "textBox3";
            this.textBox3.Top = 0F;
            this.textBox3.Width = 1F;
            // 
            // textBox4
            // 
            this.textBox4.CanGrow = false;
            this.textBox4.DataField = "Col1";
            this.textBox4.Height = 0.2F;
            this.textBox4.Left = 3F;
            this.textBox4.Name = "textBox4";
            this.textBox4.Padding = new GrapeCity.ActiveReports.PaddingEx(2, 0, 0, 0);
            this.textBox4.Style = "font-family: Microsoft YaHei";
            this.textBox4.Text = "textBox4";
            this.textBox4.Top = 0F;
            this.textBox4.Width = 1F;
            // 
            // textBox5
            // 
            this.textBox5.CanGrow = false;
            this.textBox5.DataField = "Col1";
            this.textBox5.Height = 0.2F;
            this.textBox5.Left = 4F;
            this.textBox5.Name = "textBox5";
            this.textBox5.Padding = new GrapeCity.ActiveReports.PaddingEx(2, 0, 0, 0);
            this.textBox5.Style = "font-family: Microsoft YaHei";
            this.textBox5.Text = "textBox5";
            this.textBox5.Top = 0F;
            this.textBox5.Width = 1F;
            // 
            // textBox6
            // 
            this.textBox6.CanGrow = false;
            this.textBox6.DataField = "Col1";
            this.textBox6.Height = 0.2F;
            this.textBox6.Left = 5F;
            this.textBox6.Name = "textBox6";
            this.textBox6.Padding = new GrapeCity.ActiveReports.PaddingEx(2, 0, 0, 0);
            this.textBox6.Style = "font-family: Microsoft YaHei";
            this.textBox6.Text = "textBox6";
            this.textBox6.Top = 0F;
            this.textBox6.Width = 1F;
            // 
            // pageFooter
            // 
            this.pageFooter.Name = "pageFooter";
            // 
            // line1
            // 
            this.line1.Height = 0F;
            this.line1.Left = 0F;
            this.line1.LineWeight = 1F;
            this.line1.Name = "line1";
            this.line1.Top = 0.2F;
            this.line1.Width = 6F;
            this.line1.X1 = 0F;
            this.line1.X2 = 6F;
            this.line1.Y1 = 0.2F;
            this.line1.Y2 = 0.2F;
            // 
            // shape1
            // 
            this.shape1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(163)))), ((int)(((byte)(196)))));
            this.shape1.Height = 0.2F;
            this.shape1.Left = 0F;
            this.shape1.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Transparent;
            this.shape1.Name = "shape1";
            this.shape1.RoundingRadius = 10F;
            this.shape1.Top = 0.698F;
            this.shape1.Width = 6F;
            // 
            // rptPerformance
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 6.27F;
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.pageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" +
            "l; font-size: 10pt; color: Black", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" +
            "lic", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"));
            this.ReportStart += new System.EventHandler(this.rptPerformance_ReportStart);
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox6;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;
        private GrapeCity.ActiveReports.SectionReportModel.Label label3;
        private GrapeCity.ActiveReports.SectionReportModel.Label label4;
        private GrapeCity.ActiveReports.SectionReportModel.Label label5;
        private GrapeCity.ActiveReports.SectionReportModel.Label label6;
        private GrapeCity.ActiveReports.SectionReportModel.Label label7;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape1;
    }
}
