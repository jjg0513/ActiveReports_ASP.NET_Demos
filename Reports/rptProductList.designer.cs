namespace ActiveReportsDemo.Reports
{
    /// <summary>
    /// Summary description for rptProducts1.
    /// </summary>
    partial class rptProductList
    {

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            GrapeCity.ActiveReports.Data.OleDBDataSource oleDBDataSource1 = new GrapeCity.ActiveReports.Data.OleDBDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptProductList));
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txt产品名称1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txt类别名称1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txt供应商1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt单位数量1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txt库存量1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt订购量1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt再订购量1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.picture1 = new GrapeCity.ActiveReports.SectionReportModel.Picture();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.reportInfo1 = new GrapeCity.ActiveReports.SectionReportModel.ReportInfo();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt产品名称1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt类别名称1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt供应商1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt单位数量1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt库存量1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt订购量1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt再订购量1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportInfo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detail
            // 
            this.detail.CanGrow = false;
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.label2,
            this.txt产品名称1,
            this.label3,
            this.txt类别名称1,
            this.label4,
            this.txt供应商1,
            this.txt单位数量1,
            this.label5,
            this.label6,
            this.label7,
            this.label8,
            this.txt库存量1,
            this.txt订购量1,
            this.txt再订购量1,
            this.picture1,
            this.line1,
            this.line2,
            this.line3,
            this.line4,
            this.line5,
            this.line6,
            this.line7,
            this.line8,
            this.line9,
            this.line10,
            this.line11});
            this.detail.Height = 1.5F;
            this.detail.KeepTogether = true;
            this.detail.Name = "detail";
            this.detail.Format += new System.EventHandler(this.detail_Format);
            // 
            // label2
            // 
            this.label2.ClassName = "列头-10-加粗-背景";
            this.label2.Height = 0.35F;
            this.label2.HyperLink = null;
            this.label2.Left = 0F;
            this.label2.Name = "label2";
            this.label2.Style = "background-color: #39A3C4; color: White; text-align: right; vertical-align: middl" +
    "e";
            this.label2.Text = "产品名称：";
            this.label2.Top = 0F;
            this.label2.Width = 1F;
            // 
            // txt产品名称1
            // 
            this.txt产品名称1.DataField = "产品名称";
            this.txt产品名称1.Height = 0.35F;
            this.txt产品名称1.Left = 1F;
            this.txt产品名称1.Name = "txt产品名称1";
            this.txt产品名称1.Padding = new GrapeCity.ActiveReports.PaddingEx(2, 1, 1, 1);
            this.txt产品名称1.Style = "text-align: left; vertical-align: middle";
            this.txt产品名称1.Text = "txt产品名称1";
            this.txt产品名称1.Top = 0F;
            this.txt产品名称1.Width = 2F;
            // 
            // label3
            // 
            this.label3.ClassName = "列头-10-加粗-背景";
            this.label3.Height = 0.35F;
            this.label3.HyperLink = null;
            this.label3.Left = 0F;
            this.label3.Name = "label3";
            this.label3.Style = "background-color: #39A3C4; color: White; text-align: right; vertical-align: middl" +
    "e";
            this.label3.Text = "产品类别：";
            this.label3.Top = 0.35F;
            this.label3.Width = 1F;
            // 
            // txt类别名称1
            // 
            this.txt类别名称1.DataField = "类别名称";
            this.txt类别名称1.Height = 0.35F;
            this.txt类别名称1.Left = 0.9999999F;
            this.txt类别名称1.Name = "txt类别名称1";
            this.txt类别名称1.Padding = new GrapeCity.ActiveReports.PaddingEx(2, 1, 1, 1);
            this.txt类别名称1.Style = "text-align: left; vertical-align: middle";
            this.txt类别名称1.Text = "txt类别名称1";
            this.txt类别名称1.Top = 0.35F;
            this.txt类别名称1.Width = 2F;
            // 
            // label4
            // 
            this.label4.ClassName = "列头-10-加粗-背景";
            this.label4.Height = 0.35F;
            this.label4.HyperLink = null;
            this.label4.Left = 0F;
            this.label4.Name = "label4";
            this.label4.Style = "background-color: #39A3C4; color: White; text-align: right; vertical-align: middl" +
    "e";
            this.label4.Text = "供应商信息：";
            this.label4.Top = 1.05F;
            this.label4.Width = 1F;
            // 
            // txt供应商1
            // 
            this.txt供应商1.DataField = "=供应商+ \"  \" + 联系人姓名 + \"（\" + 城市  +\"）\" + 地址";
            this.txt供应商1.Height = 0.35F;
            this.txt供应商1.Left = 1F;
            this.txt供应商1.Name = "txt供应商1";
            this.txt供应商1.Padding = new GrapeCity.ActiveReports.PaddingEx(2, 1, 1, 1);
            this.txt供应商1.Style = "text-align: left; vertical-align: middle";
            this.txt供应商1.Text = "txt供应商1";
            this.txt供应商1.Top = 1.05F;
            this.txt供应商1.Width = 5F;
            // 
            // txt单位数量1
            // 
            this.txt单位数量1.DataField = "单位数量";
            this.txt单位数量1.Height = 0.35F;
            this.txt单位数量1.Left = 1F;
            this.txt单位数量1.Name = "txt单位数量1";
            this.txt单位数量1.Padding = new GrapeCity.ActiveReports.PaddingEx(2, 1, 1, 1);
            this.txt单位数量1.Style = "text-align: left; vertical-align: middle";
            this.txt单位数量1.Text = "txt单位数量1";
            this.txt单位数量1.Top = 0.7F;
            this.txt单位数量1.Width = 2F;
            // 
            // label5
            // 
            this.label5.ClassName = "列头-10-加粗-背景";
            this.label5.Height = 0.35F;
            this.label5.HyperLink = null;
            this.label5.Left = 0F;
            this.label5.Name = "label5";
            this.label5.Style = "background-color: #39A3C4; color: White; text-align: right; vertical-align: middl" +
    "e";
            this.label5.Text = "单位数量：";
            this.label5.Top = 0.7F;
            this.label5.Width = 1F;
            // 
            // label6
            // 
            this.label6.ClassName = "列头-10-加粗-背景";
            this.label6.Height = 0.35F;
            this.label6.HyperLink = null;
            this.label6.Left = 3F;
            this.label6.Name = "label6";
            this.label6.Style = "background-color: #39A3C4; color: White; text-align: right; vertical-align: middl" +
    "e";
            this.label6.Text = "库存数量：";
            this.label6.Top = 0F;
            this.label6.Width = 1F;
            // 
            // label7
            // 
            this.label7.ClassName = "列头-10-加粗-背景";
            this.label7.Height = 0.35F;
            this.label7.HyperLink = null;
            this.label7.Left = 3.000001F;
            this.label7.Name = "label7";
            this.label7.Style = "background-color: #39A3C4; color: White; text-align: right; vertical-align: middl" +
    "e";
            this.label7.Text = "订购数量：";
            this.label7.Top = 0.35F;
            this.label7.Width = 1F;
            // 
            // label8
            // 
            this.label8.ClassName = "列头-10-加粗-背景";
            this.label8.Height = 0.35F;
            this.label8.HyperLink = null;
            this.label8.Left = 3F;
            this.label8.Name = "label8";
            this.label8.Style = "background-color: #39A3C4; color: White; text-align: right; vertical-align: middl" +
    "e";
            this.label8.Text = "再订购数量：";
            this.label8.Top = 0.7F;
            this.label8.Width = 1F;
            // 
            // txt库存量1
            // 
            this.txt库存量1.DataField = "库存量";
            this.txt库存量1.Height = 0.35F;
            this.txt库存量1.Left = 4F;
            this.txt库存量1.Name = "txt库存量1";
            this.txt库存量1.Padding = new GrapeCity.ActiveReports.PaddingEx(2, 1, 1, 1);
            this.txt库存量1.Style = "text-align: left; vertical-align: middle";
            this.txt库存量1.Text = "txt库存量1";
            this.txt库存量1.Top = 0F;
            this.txt库存量1.Width = 2F;
            // 
            // txt订购量1
            // 
            this.txt订购量1.DataField = "订购量";
            this.txt订购量1.Height = 0.35F;
            this.txt订购量1.Left = 4F;
            this.txt订购量1.Name = "txt订购量1";
            this.txt订购量1.Padding = new GrapeCity.ActiveReports.PaddingEx(2, 1, 1, 1);
            this.txt订购量1.Style = "text-align: left; vertical-align: middle";
            this.txt订购量1.Text = "txt订购量1";
            this.txt订购量1.Top = 0.35F;
            this.txt订购量1.Width = 2F;
            // 
            // txt再订购量1
            // 
            this.txt再订购量1.DataField = "再订购量";
            this.txt再订购量1.Height = 0.35F;
            this.txt再订购量1.Left = 4F;
            this.txt再订购量1.Name = "txt再订购量1";
            this.txt再订购量1.Padding = new GrapeCity.ActiveReports.PaddingEx(2, 1, 1, 1);
            this.txt再订购量1.Style = "text-align: left; vertical-align: middle";
            this.txt再订购量1.Text = "txt再订购量1";
            this.txt再订购量1.Top = 0.7F;
            this.txt再订购量1.Width = 2F;
            // 
            // picture1
            // 
            this.picture1.DataField = "图片";
            this.picture1.Height = 1.4F;
            this.picture1.ImageData = null;
            this.picture1.Left = 6F;
            this.picture1.Name = "picture1";
            this.picture1.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Stretch;
            this.picture1.Top = 0F;
            this.picture1.Width = 1.2F;
            // 
            // line1
            // 
            this.line1.Height = 0F;
            this.line1.Left = 0F;
            this.line1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(163)))), ((int)(((byte)(196)))));
            this.line1.LineWeight = 1F;
            this.line1.Name = "line1";
            this.line1.Top = 0F;
            this.line1.Width = 7.2F;
            this.line1.X1 = 0F;
            this.line1.X2 = 7.2F;
            this.line1.Y1 = 0F;
            this.line1.Y2 = 0F;
            // 
            // line2
            // 
            this.line2.Height = 0F;
            this.line2.Left = 0F;
            this.line2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(163)))), ((int)(((byte)(196)))));
            this.line2.LineWeight = 1F;
            this.line2.Name = "line2";
            this.line2.Top = 1.4F;
            this.line2.Width = 7.2F;
            this.line2.X1 = 0F;
            this.line2.X2 = 7.2F;
            this.line2.Y1 = 1.4F;
            this.line2.Y2 = 1.4F;
            // 
            // line3
            // 
            this.line3.Height = 1.4F;
            this.line3.Left = 0F;
            this.line3.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(163)))), ((int)(((byte)(196)))));
            this.line3.LineWeight = 1F;
            this.line3.Name = "line3";
            this.line3.Top = 0F;
            this.line3.Width = 0F;
            this.line3.X1 = 0F;
            this.line3.X2 = 0F;
            this.line3.Y1 = 0F;
            this.line3.Y2 = 1.4F;
            // 
            // line4
            // 
            this.line4.Height = 1.4F;
            this.line4.Left = 1F;
            this.line4.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(163)))), ((int)(((byte)(196)))));
            this.line4.LineWeight = 1F;
            this.line4.Name = "line4";
            this.line4.Top = 0F;
            this.line4.Width = 0F;
            this.line4.X1 = 1F;
            this.line4.X2 = 1F;
            this.line4.Y1 = 0F;
            this.line4.Y2 = 1.4F;
            // 
            // line5
            // 
            this.line5.Height = 1.05F;
            this.line5.Left = 3F;
            this.line5.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(163)))), ((int)(((byte)(196)))));
            this.line5.LineWeight = 1F;
            this.line5.Name = "line5";
            this.line5.Top = 0F;
            this.line5.Width = 0F;
            this.line5.X1 = 3F;
            this.line5.X2 = 3F;
            this.line5.Y1 = 0F;
            this.line5.Y2 = 1.05F;
            // 
            // line6
            // 
            this.line6.Height = 1.05F;
            this.line6.Left = 4F;
            this.line6.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(163)))), ((int)(((byte)(196)))));
            this.line6.LineWeight = 1F;
            this.line6.Name = "line6";
            this.line6.Top = 0F;
            this.line6.Width = 0F;
            this.line6.X1 = 4F;
            this.line6.X2 = 4F;
            this.line6.Y1 = 0F;
            this.line6.Y2 = 1.05F;
            // 
            // line7
            // 
            this.line7.Height = 0F;
            this.line7.Left = 0F;
            this.line7.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(163)))), ((int)(((byte)(196)))));
            this.line7.LineWeight = 1F;
            this.line7.Name = "line7";
            this.line7.Top = 0.35F;
            this.line7.Width = 6F;
            this.line7.X1 = 0F;
            this.line7.X2 = 6F;
            this.line7.Y1 = 0.35F;
            this.line7.Y2 = 0.35F;
            // 
            // line8
            // 
            this.line8.Height = 0F;
            this.line8.Left = 0F;
            this.line8.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(163)))), ((int)(((byte)(196)))));
            this.line8.LineWeight = 1F;
            this.line8.Name = "line8";
            this.line8.Top = 0.7F;
            this.line8.Width = 6F;
            this.line8.X1 = 0F;
            this.line8.X2 = 6F;
            this.line8.Y1 = 0.7F;
            this.line8.Y2 = 0.7F;
            // 
            // line9
            // 
            this.line9.Height = 0F;
            this.line9.Left = 0F;
            this.line9.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(163)))), ((int)(((byte)(196)))));
            this.line9.LineWeight = 1F;
            this.line9.Name = "line9";
            this.line9.Top = 1.05F;
            this.line9.Width = 6F;
            this.line9.X1 = 0F;
            this.line9.X2 = 6F;
            this.line9.Y1 = 1.05F;
            this.line9.Y2 = 1.05F;
            // 
            // line10
            // 
            this.line10.Height = 1.4F;
            this.line10.Left = 7.2F;
            this.line10.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(163)))), ((int)(((byte)(196)))));
            this.line10.LineWeight = 1F;
            this.line10.Name = "line10";
            this.line10.Top = 0F;
            this.line10.Width = 0F;
            this.line10.X1 = 7.2F;
            this.line10.X2 = 7.2F;
            this.line10.Y1 = 0F;
            this.line10.Y2 = 1.4F;
            // 
            // line11
            // 
            this.line11.Height = 1.4F;
            this.line11.Left = 6F;
            this.line11.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(163)))), ((int)(((byte)(196)))));
            this.line11.LineWeight = 1F;
            this.line11.Name = "line11";
            this.line11.Top = 0F;
            this.line11.Width = 0F;
            this.line11.X1 = 6F;
            this.line11.X2 = 6F;
            this.line11.Y1 = 0F;
            this.line11.Y2 = 1.4F;
            // 
            // pageHeader
            // 
            this.pageHeader.CanGrow = false;
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.label1});
            this.pageHeader.Height = 1F;
            this.pageHeader.Name = "pageHeader";
            // 
            // label1
            // 
            this.label1.CharacterSpacing = 5F;
            this.label1.ClassName = "Heading1";
            this.label1.Height = 0.783F;
            this.label1.HyperLink = null;
            this.label1.Left = 0F;
            this.label1.LineSpacing = 1F;
            this.label1.Name = "label1";
            this.label1.Style = "text-align: center; vertical-align: middle";
            this.label1.Text = "商品信息清单";
            this.label1.Top = 0F;
            this.label1.Width = 7.27F;
            // 
            // pageFooter
            // 
            this.pageFooter.CanGrow = false;
            this.pageFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.reportInfo1});
            this.pageFooter.Height = 0.4F;
            this.pageFooter.Name = "pageFooter";
            // 
            // reportInfo1
            // 
            this.reportInfo1.ClassName = "Heading3";
            this.reportInfo1.FormatString = "第 {PageNumber} 页，共 {PageCount} 页";
            this.reportInfo1.Height = 0.4F;
            this.reportInfo1.Left = 3.98F;
            this.reportInfo1.Name = "reportInfo1";
            this.reportInfo1.Style = "font-family: Microsoft YaHei; font-size: 10pt; font-weight: normal; text-align: r" +
    "ight; vertical-align: middle; ddo-char-set: 1";
            this.reportInfo1.Top = 0F;
            this.reportInfo1.Width = 3.29F;
            // 
            // rptProductList
            // 
            oleDBDataSource1.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=D:\\ActiveReports Demo\\V8\\ActiveRepor" +
    "ts for ASP.NET\\Data\\NWind_CHS.mdb;Persist Security Info=False";
            oleDBDataSource1.SQL = "SELECT TOP 15 产品.*,类别.类别名称,类别.说明 AS 类别说明,类别.图片 AS 类别图片, 供应商.公司名称 AS 供应商,供应商.联系人姓名" +
    ",供应商.城市,供应商.地址\r\nFROM (供应商 INNER JOIN 产品 ON 供应商.供应商ID = 产品.供应商ID) INNER JOIN 类别 O" +
    "N 产品.类别ID = 类别.类别ID;";
            this.DataSource = oleDBDataSource1;
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.268056F;
            this.PrintWidth = 7.27F;
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.pageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; color: Black; fon" +
            "t-family: \"Microsoft YaHei\"; font-size: 10pt; text-align: center; ddo-char-set: " +
            "162", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-weight: bold; font-family: \"Microsoft YaHei\"; font-size: 20pt; ddo-char-set:" +
            " 162", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-family: \"Microsoft YaHei\"; ddo-char-set: 162; font-style: i" +
            "nherit; font-weight: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-weight: bold; font-family: \"Microsoft YaHei\"; ddo-char-set: 162; font-size: " +
            "12pt", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; color: Black; fon" +
            "t-family: \"Microsoft YaHei\"; font-size: 10pt; ddo-char-set: 162; background-colo" +
            "r: Gainsboro", "Normal1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; color: Black; fon" +
            "t-family: \"Microsoft YaHei\"; font-size: 10pt; ddo-char-set: 162", "Normal2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "列头-加粗-背景", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; color: Black; font-family: \"Microsoft " +
            "YaHei\"; font-size: 10pt; text-align: center; ddo-char-set: 162; font-weight: bol" +
            "d; background-color: RosyBrown", "列头-10-加粗-背景", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; color: Black; fon" +
            "t-family: \"Microsoft YaHei\"; font-size: 10pt; text-align: center; ddo-char-set: " +
            "162; background-color: #FFC0C0", "行间隔色", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt产品名称1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt类别名称1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt供应商1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt单位数量1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt库存量1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt订购量1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt再订购量1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportInfo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt产品名称1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt类别名称1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt供应商1;
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;
        private GrapeCity.ActiveReports.SectionReportModel.ReportInfo reportInfo1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt单位数量1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label5;
        private GrapeCity.ActiveReports.SectionReportModel.Label label6;
        private GrapeCity.ActiveReports.SectionReportModel.Label label7;
        private GrapeCity.ActiveReports.SectionReportModel.Label label8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt库存量1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt订购量1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt再订购量1;
        private GrapeCity.ActiveReports.SectionReportModel.Picture picture1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.Line line3;
        private GrapeCity.ActiveReports.SectionReportModel.Line line4;
        private GrapeCity.ActiveReports.SectionReportModel.Line line5;
        private GrapeCity.ActiveReports.SectionReportModel.Line line6;
        private GrapeCity.ActiveReports.SectionReportModel.Line line7;
        private GrapeCity.ActiveReports.SectionReportModel.Line line8;
        private GrapeCity.ActiveReports.SectionReportModel.Line line9;
        private GrapeCity.ActiveReports.SectionReportModel.Line line10;
        private GrapeCity.ActiveReports.SectionReportModel.Line line11;
    }
}
