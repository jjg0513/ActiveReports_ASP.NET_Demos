using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace ActiveReportsDemo.Reports
{
    /// <summary>
    /// Summary description for rptMonthlySalesByCategory.
    /// </summary>
    public partial class rptMonthlySalesByCategory : SectionReportBase
    {

        public rptMonthlySalesByCategory()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
        }

        float sum1 = 0;
        float sum2 = 0;
        float sum3 = 0;

        private void detail_Format(object sender, EventArgs e)
        {
            float count,price,discount;

            if (float.TryParse(txt数量.Text, out count) && float.TryParse(txt单价.Text, out price))
            {
                float.TryParse(txt折扣.Text, out discount);
                sum1 += count * price * (1 - discount);
                sum2 += sum1;
                sum3 += sum1;
            }                       
        }

        private void groupFooter3_Format(object sender, EventArgs e)
        {
            txt销售额3.Value = sum3;
            sum3 = 0;
        }

        private void groupFooter2_Format(object sender, EventArgs e)
        {
            txt销售额2.Value = sum2;
            sum2 = 0;
        }

        private void groupFooter1_Format(object sender, EventArgs e)
        {
            txt销售额1.Value = sum1;
            sum1 = 0;
        }

        private void rptMonthlySalesByCategory_ReportStart(object sender, EventArgs e)
        {
            this.detail.Height = 0;
        }

        private void groupHeader1_Format(object sender, EventArgs e)
        {
            this.groupHeader1.AddBookmark(txt订购月1.Text);
        }

        private void groupHeader2_Format(object sender, EventArgs e)
        {
            this.groupHeader2.AddBookmark(txt订购月1.Text + "\\" + txt类别1.Text);
        }
    }
}
