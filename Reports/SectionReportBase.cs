using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace ActiveReportsDemo.Reports
{
    /// <summary>
    /// Summary description for SectionReportBase.
    /// </summary>
    public partial class SectionReportBase : GrapeCity.ActiveReports.SectionReport
    {

        public SectionReportBase()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
        }


        /// <summary>
        /// 该报表是否使用皮肤
        /// </summary>
        [DefaultValue(true)]
        public bool UseTheme
        { get; set; }

        /// <summary>
        /// 皮肤名称
        /// </summary>
        public string Theme
        { get; set; }


        int rowNo = 0;
        private void detail_Format(object sender, EventArgs e)
        {
            if ((rowNo % 2) == 0)
            {
                this.detail.BackColor = (Color)this.StyleSheet["行间隔色"].BackColor;
            }
            else
            {
                this.detail.BackColor = Color.Transparent;
            }

            rowNo += 1;
        }

        private void SectionReportBase_ReportStart(object sender, EventArgs e)
        {
            this.Document.Printer.PrinterName = "";

            if (UseTheme)
            {
                string path = @"Theme\" +  (string.IsNullOrEmpty(Theme) ? "DefaultStyle.reportstyle" : Theme);
                this.LoadStyles( path);
            }
        }
    }
}
