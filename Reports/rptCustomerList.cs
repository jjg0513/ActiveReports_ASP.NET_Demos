using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace ActiveReportsDemo.Reports
{
    /// <summary>
    /// Summary description for rptCustomerList.
    /// </summary>
    public partial class rptCustomerList : SectionReportBase
    {

        public rptCustomerList()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
        }

        int rowNo = 0;
        private void detail_Format(object sender, EventArgs e)
        {
            if ((rowNo % 2) == 0)
            {
                this.detail.BackColor = (Color)this.StyleSheet["�м��ɫ"].BackColor;
            }
            else
            {
                this.detail.BackColor = Color.Transparent;
            }

            rowNo += 1;
        }
    }
}
