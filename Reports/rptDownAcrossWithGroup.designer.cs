namespace ActiveReportsDemo.Reports
{
    /// <summary>
    /// Summary description for rptDownAcross.
    /// </summary>
    partial class rptDownAcrossWithGroup
    {

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            GrapeCity.ActiveReports.Data.OleDBDataSource oleDBDataSource1 = new GrapeCity.ActiveReports.Data.OleDBDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptDownAcrossWithGroup));
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.txt项目编号1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt项目编号2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt数量1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.checkBox1 = new GrapeCity.ActiveReports.SectionReportModel.CheckBox();
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.reportInfo1 = new GrapeCity.ActiveReports.SectionReportModel.ReportInfo();
            this.groupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.groupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            ((System.ComponentModel.ISupportInitialize)(this.txt项目编号1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt项目编号2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt数量1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportInfo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detail
            // 
            this.detail.CanGrow = false;
            this.detail.ColumnCount = 2;
            this.detail.ColumnSpacing = 0.27F;
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.shape1,
            this.txt项目编号1,
            this.txt项目编号2,
            this.txt数量1,
            this.checkBox1});
            this.detail.Height = 0.25F;
            this.detail.KeepTogether = true;
            this.detail.Name = "detail";
            this.detail.Format += new System.EventHandler(this.detail_Format);
            // 
            // shape1
            // 
            this.shape1.Height = 0.25F;
            this.shape1.Left = 0F;
            this.shape1.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Transparent;
            this.shape1.LineWeight = 0F;
            this.shape1.Name = "shape1";
            this.shape1.RoundingRadius = 9.999999F;
            this.shape1.Top = 0F;
            this.shape1.Width = 3.5F;
            // 
            // txt项目编号1
            // 
            this.txt项目编号1.DataField = "行号";
            this.txt项目编号1.Height = 0.25F;
            this.txt项目编号1.Left = 0F;
            this.txt项目编号1.Name = "txt项目编号1";
            this.txt项目编号1.Style = "vertical-align: middle";
            this.txt项目编号1.Text = "No";
            this.txt项目编号1.Top = 0F;
            this.txt项目编号1.Width = 0.2F;
            // 
            // txt项目编号2
            // 
            this.txt项目编号2.DataField = "项目名称";
            this.txt项目编号2.Height = 0.25F;
            this.txt项目编号2.Left = 0.2F;
            this.txt项目编号2.Name = "txt项目编号2";
            this.txt项目编号2.Padding = new GrapeCity.ActiveReports.PaddingEx(2, 0, 0, 0);
            this.txt项目编号2.Style = "text-align: left; vertical-align: middle";
            this.txt项目编号2.Text = "txt项目编号2";
            this.txt项目编号2.Top = 0F;
            this.txt项目编号2.Width = 2F;
            // 
            // txt数量1
            // 
            this.txt数量1.DataField = "数量";
            this.txt数量1.Height = 0.25F;
            this.txt数量1.Left = 2.2F;
            this.txt数量1.Name = "txt数量1";
            this.txt数量1.Style = "text-align: right; vertical-align: middle";
            this.txt数量1.Text = "txt数1";
            this.txt数量1.Top = 0F;
            this.txt数量1.Width = 0.5F;
            // 
            // checkBox1
            // 
            this.checkBox1.Height = 0.25F;
            this.checkBox1.Left = 3.064F;
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Style = "";
            this.checkBox1.Text = "";
            this.checkBox1.Top = 0F;
            this.checkBox1.Width = 0.155F;
            // 
            // pageHeader
            // 
            this.pageHeader.CanGrow = false;
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.label1});
            this.pageHeader.Height = 0.635F;
            this.pageHeader.Name = "pageHeader";
            // 
            // label1
            // 
            this.label1.CharacterSpacing = 5F;
            this.label1.ClassName = "Heading1";
            this.label1.Height = 0.5F;
            this.label1.HyperLink = null;
            this.label1.Left = 0F;
            this.label1.LineSpacing = 1F;
            this.label1.Name = "label1";
            this.label1.Style = "text-align: center; vertical-align: middle";
            this.label1.Text = "2013年欧洲十国游物品整理清单";
            this.label1.Top = 0F;
            this.label1.Width = 7.27F;
            // 
            // pageFooter
            // 
            this.pageFooter.CanGrow = false;
            this.pageFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.reportInfo1});
            this.pageFooter.Height = 0.3F;
            this.pageFooter.Name = "pageFooter";
            // 
            // reportInfo1
            // 
            this.reportInfo1.ClassName = "Heading3";
            this.reportInfo1.FormatString = "第 {PageNumber} 页，共 {PageCount} 页";
            this.reportInfo1.Height = 0.3F;
            this.reportInfo1.Left = 3.98F;
            this.reportInfo1.Name = "reportInfo1";
            this.reportInfo1.Style = "font-family: Microsoft YaHei; font-size: 10pt; font-weight: normal; text-align: r" +
    "ight; vertical-align: middle; ddo-char-set: 1";
            this.reportInfo1.Top = 0F;
            this.reportInfo1.Width = 3.29F;
            // 
            // groupHeader1
            // 
            this.groupHeader1.ColumnGroupKeepTogether = true;
            this.groupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox1,
            this.label2,
            this.label3,
            this.label4});
            this.groupHeader1.DataField = "类别名称";
            this.groupHeader1.GroupKeepTogether = GrapeCity.ActiveReports.SectionReportModel.GroupKeepTogether.FirstDetail;
            this.groupHeader1.Height = 0.55F;
            this.groupHeader1.KeepTogether = true;
            this.groupHeader1.Name = "groupHeader1";
            this.groupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnColumn;
            // 
            // textBox1
            // 
            this.textBox1.ClassName = "列头-加粗-背景";
            this.textBox1.DataField = "=类别名称 + \"  (\" + 必需性 + \")\"";
            this.textBox1.Height = 0.3F;
            this.textBox1.Left = 0F;
            this.textBox1.Name = "textBox1";
            this.textBox1.Padding = new GrapeCity.ActiveReports.PaddingEx(2, 1, 1, 1);
            this.textBox1.Style = "background-color: #39A3C4; color: White; text-align: left";
            this.textBox1.Text = "textBox1";
            this.textBox1.Top = 0F;
            this.textBox1.Width = 3.5F;
            // 
            // label2
            // 
            this.label2.Height = 0.25F;
            this.label2.HyperLink = null;
            this.label2.Left = 0F;
            this.label2.Name = "label2";
            this.label2.Padding = new GrapeCity.ActiveReports.PaddingEx(16, 0, 0, 0);
            this.label2.Style = "background-color: DarkGray; color: White; text-align: left; vertical-align: middl" +
    "e";
            this.label2.Text = "物品名";
            this.label2.Top = 0.3F;
            this.label2.Width = 2.2F;
            // 
            // label3
            // 
            this.label3.Height = 0.25F;
            this.label3.HyperLink = null;
            this.label3.Left = 2.2F;
            this.label3.Name = "label3";
            this.label3.Padding = new GrapeCity.ActiveReports.PaddingEx(2, 0, 0, 0);
            this.label3.Style = "background-color: DarkGray; color: White; text-align: center; vertical-align: mid" +
    "dle";
            this.label3.Text = "数量";
            this.label3.Top = 0.3F;
            this.label3.Width = 0.5F;
            // 
            // label4
            // 
            this.label4.Height = 0.25F;
            this.label4.HyperLink = null;
            this.label4.Left = 2.7F;
            this.label4.Name = "label4";
            this.label4.Padding = new GrapeCity.ActiveReports.PaddingEx(2, 0, 0, 0);
            this.label4.Style = "background-color: DarkGray; color: White; text-align: center; vertical-align: mid" +
    "dle";
            this.label4.Text = "已打包";
            this.label4.Top = 0.3F;
            this.label4.Width = 0.8F;
            // 
            // groupFooter1
            // 
            this.groupFooter1.Height = 0.2F;
            this.groupFooter1.Name = "groupFooter1";
            // 
            // rptDownAcrossWithGroup
            // 
            oleDBDataSource1.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=D:\\ActiveReports Demo\\V8\\ActiveRepor" +
    "ts for ASP.NET\\Data\\NWind_CHS.mdb;Persist Security Info=False";
            oleDBDataSource1.SQL = "SELECT *   FROM 物品清单\r\nORDER BY 行号";
            this.DataSource = oleDBDataSource1;
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.268056F;
            this.PrintWidth = 7.27F;
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.groupHeader1);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.groupFooter1);
            this.Sections.Add(this.pageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; color: Black; fon" +
            "t-family: \"Microsoft YaHei\"; font-size: 10pt; text-align: center; ddo-char-set: " +
            "162", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-weight: bold; font-family: \"Microsoft YaHei\"; font-size: 20pt; ddo-char-set:" +
            " 162", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-family: \"Microsoft YaHei\"; ddo-char-set: 162; font-style: i" +
            "nherit; font-weight: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-weight: bold; font-family: \"Microsoft YaHei\"; ddo-char-set: 162; font-size: " +
            "12pt", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; color: Black; fon" +
            "t-family: \"Microsoft YaHei\"; font-size: 10pt; ddo-char-set: 162; background-colo" +
            "r: Gainsboro", "Normal1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; color: Black; fon" +
            "t-family: \"Microsoft YaHei\"; font-size: 10pt; ddo-char-set: 162", "Normal2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "列头-加粗-背景", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; color: Black; font-family: \"Microsoft " +
            "YaHei\"; font-size: 10pt; text-align: center; ddo-char-set: 162; font-weight: bol" +
            "d; background-color: RosyBrown", "列头-10-加粗-背景", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; color: Black; fon" +
            "t-family: \"Microsoft YaHei\"; font-size: 10pt; text-align: center; ddo-char-set: " +
            "162; background-color: #FFC0C0", "行间隔色", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txt项目编号1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt项目编号2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt数量1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportInfo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader groupHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter groupFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;
        private GrapeCity.ActiveReports.SectionReportModel.Label label3;
        private GrapeCity.ActiveReports.SectionReportModel.Label label4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt项目编号1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt项目编号2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt数量1;
        private GrapeCity.ActiveReports.SectionReportModel.CheckBox checkBox1;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape1;
        private GrapeCity.ActiveReports.SectionReportModel.ReportInfo reportInfo1;
    }
}
