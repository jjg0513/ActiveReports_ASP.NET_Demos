using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace ActiveReportsDemo.Reports
{
    /// <summary>
    /// Summary description for rptMultiSeries.
    /// </summary>
    public partial class rptMultiSeries : GrapeCity.ActiveReports.SectionReport
    {

        public rptMultiSeries()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
        }

        private void rptMultiSeries_ReportStart(object sender, EventArgs e)
        {
            this.Document.Printer.PrinterName = "";
        }
    }
}
