namespace ActiveReportsDemo.Reports
{
    /// <summary>
    /// Summary description for rptBudgetList.
    /// </summary>
    partial class rptBudgetList
    {

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            GrapeCity.ActiveReports.Data.OleDBDataSource oleDBDataSource1 = new GrapeCity.ActiveReports.Data.OleDBDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptBudgetList));
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txt˵��1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.shape2 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.txt�����1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt�����1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt����1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtԤ��1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt��Ŀ���1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.crossSectionLine2 = new GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine();
            this.crossSectionLine3 = new GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine();
            this.crossSectionLine4 = new GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine();
            this.crossSectionLine1 = new GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine();
            this.crossSectionLine6 = new GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine();
            this.crossSectionLine5 = new GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.crossSectionLine7 = new GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine();
            this.crossSectionLine8 = new GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.reportInfo2 = new GrapeCity.ActiveReports.SectionReportModel.ReportInfo();
            this.line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            ((System.ComponentModel.ISupportInitialize)(this.txt˵��1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt�����1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt�����1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt����1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtԤ��1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt��Ŀ���1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportInfo2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detail
            // 
            this.detail.CanGrow = false;
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txt˵��1,
            this.shape2,
            this.txt�����1,
            this.txt�����1,
            this.txt����1,
            this.txtԤ��1,
            this.txt��Ŀ���1,
            this.line5});
            this.detail.Height = 0.25F;
            this.detail.KeepTogether = true;
            this.detail.Name = "detail";
            this.detail.RepeatToFill = true;
            this.detail.Format += new System.EventHandler(this.detail_Format);
            // 
            // txt˵��1
            // 
            this.txt˵��1.ClassName = "Normal2";
            this.txt˵��1.DataField = "˵��";
            this.txt˵��1.Height = 0.25F;
            this.txt˵��1.Left = 7.4F;
            this.txt˵��1.Name = "txt˵��1";
            this.txt˵��1.OutputFormat = resources.GetString("txt˵��1.OutputFormat");
            this.txt˵��1.Padding = new GrapeCity.ActiveReports.PaddingEx(5, 2, 2, 2);
            this.txt˵��1.Style = "font-family: Microsoft YaHei; font-size: 11pt; text-align: left; ddo-char-set: 1";
            this.txt˵��1.Text = "txt˵��1";
            this.txt˵��1.Top = 0F;
            this.txt˵��1.Width = 2.019F;
            // 
            // shape2
            // 
            this.shape2.Height = 0.25F;
            this.shape2.Left = 6.4F;
            this.shape2.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Transparent;
            this.shape2.Name = "shape2";
            this.shape2.RoundingRadius = 9.999999F;
            this.shape2.Top = 0F;
            this.shape2.Width = 1F;
            // 
            // txt�����1
            // 
            this.txt�����1.ClassName = "Normal2";
            this.txt�����1.Height = 0.25F;
            this.txt�����1.Left = 5.4F;
            this.txt�����1.Name = "txt�����1";
            this.txt�����1.OutputFormat = resources.GetString("txt�����1.OutputFormat");
            this.txt�����1.Padding = new GrapeCity.ActiveReports.PaddingEx(2, 2, 5, 2);
            this.txt�����1.Style = "font-family: Microsoft YaHei; font-size: 11pt; text-align: right; ddo-char-set: 1" +
    "";
            this.txt�����1.Text = "txt�����1";
            this.txt�����1.Top = 0F;
            this.txt�����1.Width = 1F;
            // 
            // txt�����1
            // 
            this.txt�����1.ClassName = "Normal2";
            this.txt�����1.Height = 0.25F;
            this.txt�����1.Left = 4.4F;
            this.txt�����1.Name = "txt�����1";
            this.txt�����1.OutputFormat = resources.GetString("txt�����1.OutputFormat");
            this.txt�����1.Padding = new GrapeCity.ActiveReports.PaddingEx(2, 2, 5, 2);
            this.txt�����1.Style = "font-family: Microsoft YaHei; font-size: 11pt; text-align: right; ddo-char-set: 1" +
    "";
            this.txt�����1.Text = "txt�����1";
            this.txt�����1.Top = 0F;
            this.txt�����1.Width = 1F;
            // 
            // txt����1
            // 
            this.txt����1.ClassName = "Normal2";
            this.txt����1.DataField = "����";
            this.txt����1.Height = 0.25F;
            this.txt����1.Left = 3.2F;
            this.txt����1.Name = "txt����1";
            this.txt����1.OutputFormat = resources.GetString("txt����1.OutputFormat");
            this.txt����1.Padding = new GrapeCity.ActiveReports.PaddingEx(2, 2, 5, 2);
            this.txt����1.Style = "font-family: Microsoft YaHei; font-size: 11pt; text-align: right; ddo-char-set: 1" +
    "";
            this.txt����1.Text = "txt����1";
            this.txt����1.Top = 0F;
            this.txt����1.Width = 1.2F;
            // 
            // txtԤ��1
            // 
            this.txtԤ��1.ClassName = "Normal2";
            this.txtԤ��1.DataField = "Ԥ��";
            this.txtԤ��1.Height = 0.25F;
            this.txtԤ��1.Left = 2F;
            this.txtԤ��1.Name = "txtԤ��1";
            this.txtԤ��1.OutputFormat = resources.GetString("txtԤ��1.OutputFormat");
            this.txtԤ��1.Padding = new GrapeCity.ActiveReports.PaddingEx(2, 2, 5, 2);
            this.txtԤ��1.Style = "font-family: Microsoft YaHei; font-size: 11pt; text-align: right; ddo-char-set: 1" +
    "";
            this.txtԤ��1.Text = "txtԤ��1";
            this.txtԤ��1.Top = 0F;
            this.txtԤ��1.Width = 1.2F;
            // 
            // txt��Ŀ���1
            // 
            this.txt��Ŀ���1.ClassName = "Normal1";
            this.txt��Ŀ���1.DataField = "=��Ŀ���+\"        \"+��Ŀ����";
            this.txt��Ŀ���1.Height = 0.25F;
            this.txt��Ŀ���1.Left = 0F;
            this.txt��Ŀ���1.Name = "txt��Ŀ���1";
            this.txt��Ŀ���1.Padding = new GrapeCity.ActiveReports.PaddingEx(5, 2, 2, 2);
            this.txt��Ŀ���1.Style = "background-color: #39A3C4; color: White; font-family: Microsoft YaHei; font-size:" +
    " 11pt; font-weight: bold; ddo-char-set: 1";
            this.txt��Ŀ���1.Text = "txt��Ŀ���1";
            this.txt��Ŀ���1.Top = 0F;
            this.txt��Ŀ���1.Width = 2F;
            // 
            // line5
            // 
            this.line5.Height = 0F;
            this.line5.Left = 0F;
            this.line5.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.line5.LineWeight = 2F;
            this.line5.Name = "line5";
            this.line5.Top = 0F;
            this.line5.Width = 9.419001F;
            this.line5.X1 = 0F;
            this.line5.X2 = 9.419001F;
            this.line5.Y1 = 0F;
            this.line5.Y2 = 0F;
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.label1,
            this.label2,
            this.label4,
            this.label5,
            this.label6,
            this.label7,
            this.label8,
            this.label9,
            this.label10,
            this.crossSectionLine2,
            this.crossSectionLine3,
            this.crossSectionLine4,
            this.crossSectionLine1,
            this.crossSectionLine6,
            this.crossSectionLine5,
            this.line2,
            this.label3,
            this.line3,
            this.crossSectionLine7,
            this.crossSectionLine8});
            this.pageHeader.Height = 1.360417F;
            this.pageHeader.Name = "pageHeader";
            // 
            // label1
            // 
            this.label1.CharacterSpacing = 10F;
            this.label1.ClassName = "Heading1";
            this.label1.Height = 0.5F;
            this.label1.HyperLink = null;
            this.label1.Left = 0F;
            this.label1.LineSpacing = 1F;
            this.label1.Name = "label1";
            this.label1.Style = "font-family: Microsoft YaHei; font-size: 20pt; font-weight: bold; text-align: cen" +
    "ter; vertical-align: middle";
            this.label1.Text = "֧��Ԥ����ϸ��";
            this.label1.Top = 0F;
            this.label1.Width = 9.409F;
            // 
            // label2
            // 
            this.label2.ClassName = "Heading2";
            this.label2.Height = 0.3170001F;
            this.label2.HyperLink = null;
            this.label2.Left = 2.596F;
            this.label2.Name = "label2";
            this.label2.Style = "font-family: Microsoft YaHei; font-size: 14pt; text-align: center";
            this.label2.Text = "2011��01��01�� ���� 2011��12��31��";
            this.label2.Top = 0.5F;
            this.label2.Width = 4.200001F;
            // 
            // label4
            // 
            this.label4.ClassName = "Normal1";
            this.label4.Height = 0.5F;
            this.label4.HyperLink = null;
            this.label4.Left = 2F;
            this.label4.Name = "label4";
            this.label4.Style = "background-color: #39A3C4; color: White; font-family: Microsoft YaHei; font-size:" +
    " 12pt; font-weight: bold; text-align: center; vertical-align: middle; ddo-char-s" +
    "et: 1";
            this.label4.Text = "�����Ԥ��";
            this.label4.Top = 0.85F;
            this.label4.Width = 1.2F;
            // 
            // label5
            // 
            this.label5.ClassName = "Normal1";
            this.label5.Height = 0.5F;
            this.label5.HyperLink = null;
            this.label5.Left = 3.2F;
            this.label5.Name = "label5";
            this.label5.Style = "background-color: #39A3C4; color: White; font-family: Microsoft YaHei; font-size:" +
    " 12pt; font-weight: bold; text-align: center; vertical-align: middle; ddo-char-s" +
    "et: 1";
            this.label5.Text = "�����Ԥ��\r\n����";
            this.label5.Top = 0.85F;
            this.label5.Width = 1.2F;
            // 
            // label6
            // 
            this.label6.ClassName = "Normal1";
            this.label6.Height = 0.25F;
            this.label6.HyperLink = null;
            this.label6.Left = 4.4F;
            this.label6.Name = "label6";
            this.label6.Style = "background-color: #39A3C4; color: White; font-family: Microsoft YaHei; font-size:" +
    " 12pt; font-weight: bold; text-align: center; vertical-align: middle; ddo-char-s" +
    "et: 1";
            this.label6.Text = "�����Ԥ��";
            this.label6.Top = 0.85F;
            this.label6.Width = 3F;
            // 
            // label7
            // 
            this.label7.ClassName = "Normal1";
            this.label7.Height = 0.25F;
            this.label7.HyperLink = null;
            this.label7.Left = 4.4F;
            this.label7.Name = "label7";
            this.label7.Style = "background-color: #39A3C4; color: White; font-family: Microsoft YaHei; font-size:" +
    " 12pt; font-weight: bold; text-align: center; vertical-align: middle; ddo-char-s" +
    "et: 1";
            this.label7.Text = "����� ��";
            this.label7.Top = 1.1F;
            this.label7.Width = 1F;
            // 
            // label8
            // 
            this.label8.ClassName = "Normal1";
            this.label8.Height = 0.25F;
            this.label8.HyperLink = null;
            this.label8.Left = 5.4F;
            this.label8.Name = "label8";
            this.label8.Style = "background-color: #39A3C4; color: White; font-family: Microsoft YaHei; font-size:" +
    " 12pt; font-weight: bold; text-align: center; vertical-align: middle; ddo-char-s" +
    "et: 1";
            this.label8.Text = "����� %";
            this.label8.Top = 1.1F;
            this.label8.Width = 1F;
            // 
            // label9
            // 
            this.label9.ClassName = "Normal1";
            this.label9.Height = 0.25F;
            this.label9.HyperLink = null;
            this.label9.Left = 6.4F;
            this.label9.Name = "label9";
            this.label9.Style = "background-color: #39A3C4; color: White; font-family: Microsoft YaHei; font-size:" +
    " 12pt; font-weight: bold; text-align: center; vertical-align: middle; ddo-char-s" +
    "et: 1";
            this.label9.Text = "����ͼ";
            this.label9.Top = 1.1F;
            this.label9.Width = 1F;
            // 
            // label10
            // 
            this.label10.CharacterSpacing = 50F;
            this.label10.ClassName = "Normal1";
            this.label10.Height = 0.5F;
            this.label10.HyperLink = null;
            this.label10.Left = 7.4F;
            this.label10.Name = "label10";
            this.label10.Style = "background-color: #39A3C4; color: White; font-family: Microsoft YaHei; font-size:" +
    " 12pt; font-weight: bold; text-align: center; vertical-align: middle; ddo-char-s" +
    "et: 1";
            this.label10.Text = "˵��";
            this.label10.Top = 0.85F;
            this.label10.Width = 2.019F;
            // 
            // crossSectionLine2
            // 
            this.crossSectionLine2.Bottom = 0F;
            this.crossSectionLine2.Left = 2F;
            this.crossSectionLine2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.crossSectionLine2.LineWeight = 2F;
            this.crossSectionLine2.Name = "crossSectionLine2";
            this.crossSectionLine2.Top = 0.85F;
            // 
            // crossSectionLine3
            // 
            this.crossSectionLine3.Bottom = 0F;
            this.crossSectionLine3.Left = 3.2F;
            this.crossSectionLine3.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.crossSectionLine3.LineWeight = 2F;
            this.crossSectionLine3.Name = "crossSectionLine3";
            this.crossSectionLine3.Top = 0.85F;
            // 
            // crossSectionLine4
            // 
            this.crossSectionLine4.Bottom = 0F;
            this.crossSectionLine4.Left = 4.4F;
            this.crossSectionLine4.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.crossSectionLine4.LineWeight = 2F;
            this.crossSectionLine4.Name = "crossSectionLine4";
            this.crossSectionLine4.Top = 0.85F;
            // 
            // crossSectionLine1
            // 
            this.crossSectionLine1.Bottom = 0F;
            this.crossSectionLine1.Left = 5.4F;
            this.crossSectionLine1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.crossSectionLine1.LineWeight = 2F;
            this.crossSectionLine1.Name = "crossSectionLine1";
            this.crossSectionLine1.Top = 1.35F;
            // 
            // crossSectionLine6
            // 
            this.crossSectionLine6.Bottom = 0F;
            this.crossSectionLine6.Left = 6.4F;
            this.crossSectionLine6.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.crossSectionLine6.LineWeight = 2F;
            this.crossSectionLine6.Name = "crossSectionLine6";
            this.crossSectionLine6.Top = 1.35F;
            // 
            // crossSectionLine5
            // 
            this.crossSectionLine5.Bottom = 0F;
            this.crossSectionLine5.Left = 7.4F;
            this.crossSectionLine5.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.crossSectionLine5.LineWeight = 2F;
            this.crossSectionLine5.Name = "crossSectionLine5";
            this.crossSectionLine5.Top = 0.85F;
            // 
            // line2
            // 
            this.line2.Height = 0F;
            this.line2.Left = 4.4F;
            this.line2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.line2.LineWeight = 2F;
            this.line2.Name = "line2";
            this.line2.Top = 1.35F;
            this.line2.Width = 3F;
            this.line2.X1 = 4.4F;
            this.line2.X2 = 7.4F;
            this.line2.Y1 = 1.35F;
            this.line2.Y2 = 1.35F;
            // 
            // label3
            // 
            this.label3.ClassName = "Normal1";
            this.label3.Height = 0.5F;
            this.label3.HyperLink = null;
            this.label3.Left = 0F;
            this.label3.Name = "label3";
            this.label3.Style = "background-color: #39A3C4; color: White; font-family: Microsoft YaHei; font-size:" +
    " 12pt; font-weight: bold; text-align: center; vertical-align: middle; ddo-char-s" +
    "et: 1";
            this.label3.Text = "��Ʊ�ſ�Ŀ";
            this.label3.Top = 0.85F;
            this.label3.Width = 2F;
            // 
            // line3
            // 
            this.line3.Height = 0F;
            this.line3.Left = 4.4F;
            this.line3.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.line3.LineWeight = 2F;
            this.line3.Name = "line3";
            this.line3.Top = 1.1F;
            this.line3.Width = 3F;
            this.line3.X1 = 4.4F;
            this.line3.X2 = 7.4F;
            this.line3.Y1 = 1.1F;
            this.line3.Y2 = 1.1F;
            // 
            // crossSectionLine7
            // 
            this.crossSectionLine7.Bottom = 0F;
            this.crossSectionLine7.Left = 0F;
            this.crossSectionLine7.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.crossSectionLine7.LineWeight = 2F;
            this.crossSectionLine7.Name = "crossSectionLine7";
            this.crossSectionLine7.Top = 1.1F;
            // 
            // crossSectionLine8
            // 
            this.crossSectionLine8.Bottom = 0F;
            this.crossSectionLine8.Left = 9.419001F;
            this.crossSectionLine8.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.crossSectionLine8.LineWeight = 2F;
            this.crossSectionLine8.Name = "crossSectionLine8";
            this.crossSectionLine8.Top = 0.85F;
            // 
            // pageFooter
            // 
            this.pageFooter.CanGrow = false;
            this.pageFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.reportInfo2,
            this.line4});
            this.pageFooter.Height = 0.36F;
            this.pageFooter.Name = "pageFooter";
            // 
            // reportInfo2
            // 
            this.reportInfo2.ClassName = "Heading3";
            this.reportInfo2.FormatString = "�� {PageNumber} ҳ���� {PageCount} ҳ";
            this.reportInfo2.Height = 0.36F;
            this.reportInfo2.Left = 6.218999F;
            this.reportInfo2.Name = "reportInfo2";
            this.reportInfo2.Style = "font-family: Microsoft YaHei; font-size: 10pt; font-weight: normal; text-align: r" +
    "ight; vertical-align: bottom; ddo-char-set: 1";
            this.reportInfo2.Top = 0F;
            this.reportInfo2.Width = 3.2F;
            // 
            // line4
            // 
            this.line4.Height = 0F;
            this.line4.Left = 0F;
            this.line4.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.line4.LineWeight = 2F;
            this.line4.Name = "line4";
            this.line4.Top = 0F;
            this.line4.Width = 9.419001F;
            this.line4.X1 = 0F;
            this.line4.X2 = 9.419001F;
            this.line4.Y1 = 0F;
            this.line4.Y2 = 0F;
            // 
            // rptBudgetList
            // 
            oleDBDataSource1.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=D:\\ActiveReports Demo\\V8\\ActiveRepor" +
    "ts for ASP.NET\\Data\\NWind_CHS.mdb;Persist Security Info=False";
            oleDBDataSource1.SQL = "Select * from ֧��Ԥ��";
            this.DataSource = oleDBDataSource1;
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.3F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.2201389F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 10.5F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Custom;
            this.PageSettings.PaperName = "�Զ���ֽ��";
            this.PageSettings.PaperWidth = 7F;
            this.PrintWidth = 9.5F;
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.pageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; color: Black; fon" +
            "t-family: \"Microsoft YaHei\"; font-size: 10pt; text-align: center; ddo-char-set: " +
            "162", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-weight: bold; font-family: \"Microsoft YaHei\"; font-size: 20pt; ddo-char-set:" +
            " 162", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-family: \"Microsoft YaHei\"; ddo-char-set: 162; font-style: i" +
            "nherit; font-weight: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-weight: bold; font-family: \"Microsoft YaHei\"; ddo-char-set: 162; font-size: " +
            "12pt", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; color: Black; fon" +
            "t-family: \"Microsoft YaHei\"; font-size: 10pt; ddo-char-set: 162; background-colo" +
            "r: Gainsboro", "Normal1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; color: Black; fon" +
            "t-family: \"Microsoft YaHei\"; font-size: 10pt; ddo-char-set: 162", "Normal2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "��ͷ-�Ӵ�-����", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; color: Black; font-family: \"Microsoft " +
            "YaHei\"; font-size: 10pt; text-align: center; ddo-char-set: 162; font-weight: bol" +
            "d; background-color: #CD6839", "��ͷ-10-�Ӵ�-����", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; color: Black; fon" +
            "t-family: \"Microsoft YaHei\"; font-size: 10pt; text-align: center; ddo-char-set: " +
            "162; background-color: #C7C7C7", "�м��ɫ", "Normal"));
            this.ReportStart += new System.EventHandler(this.rptBudgetList_ReportStart);
            ((System.ComponentModel.ISupportInitialize)(this.txt˵��1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt�����1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt�����1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt����1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtԤ��1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt��Ŀ���1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportInfo2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt˵��1;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt�����1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt�����1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt����1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtԤ��1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt��Ŀ���1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line5;
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;
        private GrapeCity.ActiveReports.SectionReportModel.Label label4;
        private GrapeCity.ActiveReports.SectionReportModel.Label label5;
        private GrapeCity.ActiveReports.SectionReportModel.Label label6;
        private GrapeCity.ActiveReports.SectionReportModel.Label label7;
        private GrapeCity.ActiveReports.SectionReportModel.Label label8;
        private GrapeCity.ActiveReports.SectionReportModel.Label label9;
        private GrapeCity.ActiveReports.SectionReportModel.Label label10;
        private GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine crossSectionLine2;
        private GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine crossSectionLine3;
        private GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine crossSectionLine4;
        private GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine crossSectionLine1;
        private GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine crossSectionLine6;
        private GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine crossSectionLine5;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.Label label3;
        private GrapeCity.ActiveReports.SectionReportModel.Line line3;
        private GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine crossSectionLine7;
        private GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine crossSectionLine8;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;
        private GrapeCity.ActiveReports.SectionReportModel.ReportInfo reportInfo2;
        private GrapeCity.ActiveReports.SectionReportModel.Line line4;

    }
}
