namespace ActiveReportsDemo.Reports
{
    /// <summary>
    /// Summary description for rptProductsByCategory.
    /// </summary>
    partial class rptProductsByCategory
    {

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            GrapeCity.ActiveReports.Data.OleDBDataSource oleDBDataSource1 = new GrapeCity.ActiveReports.Data.OleDBDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptProductsByCategory));
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.picture1 = new GrapeCity.ActiveReports.SectionReportModel.Picture();
            this.txt产品名称1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt单位数量1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt单价1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt订购量1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt库存量1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt再订购量1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt中止1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.reportInfo1 = new GrapeCity.ActiveReports.SectionReportModel.ReportInfo();
            this.groupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.txt类别名称1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.groupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txt产品ID1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txt单价2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            ((System.ComponentModel.ISupportInitialize)(this.picture1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt产品名称1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt单位数量1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt单价1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt订购量1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt库存量1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt再订购量1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt中止1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportInfo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt类别名称1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt产品ID1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt单价2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detail
            // 
            this.detail.CanGrow = false;
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.picture1,
            this.txt产品名称1,
            this.txt单位数量1,
            this.txt单价1,
            this.txt订购量1,
            this.txt库存量1,
            this.txt再订购量1,
            this.txt中止1,
            this.textBox1});
            this.detail.Height = 0.3F;
            this.detail.KeepTogether = true;
            this.detail.Name = "detail";
            this.detail.Format += new System.EventHandler(this.detail_Format);
            // 
            // picture1
            // 
            this.picture1.DataField = "图片";
            this.picture1.Height = 0.3F;
            this.picture1.HyperLink = null;
            this.picture1.ImageData = null;
            this.picture1.Left = 0F;
            this.picture1.Name = "picture1";
            this.picture1.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
            this.picture1.Top = 0F;
            this.picture1.Width = 0.3F;
            // 
            // txt产品名称1
            // 
            this.txt产品名称1.DataField = "产品名称";
            this.txt产品名称1.Height = 0.3F;
            this.txt产品名称1.Left = 0.3F;
            this.txt产品名称1.Name = "txt产品名称1";
            this.txt产品名称1.Padding = new GrapeCity.ActiveReports.PaddingEx(3, 1, 1, 1);
            this.txt产品名称1.Style = "text-align: left; vertical-align: middle";
            this.txt产品名称1.Text = "txt产品名称1";
            this.txt产品名称1.Top = 0F;
            this.txt产品名称1.Width = 1F;
            // 
            // txt单位数量1
            // 
            this.txt单位数量1.DataField = "单位数量";
            this.txt单位数量1.Height = 0.3F;
            this.txt单位数量1.Left = 1.3F;
            this.txt单位数量1.Name = "txt单位数量1";
            this.txt单位数量1.Padding = new GrapeCity.ActiveReports.PaddingEx(1, 1, 1, 1);
            this.txt单位数量1.Style = "vertical-align: middle";
            this.txt单位数量1.Text = "txt单位数量1";
            this.txt单位数量1.Top = 0F;
            this.txt单位数量1.Width = 1.2F;
            // 
            // txt单价1
            // 
            this.txt单价1.DataField = "单价";
            this.txt单价1.Height = 0.3F;
            this.txt单价1.Left = 2.5F;
            this.txt单价1.Name = "txt单价1";
            this.txt单价1.OutputFormat = resources.GetString("txt单价1.OutputFormat");
            this.txt单价1.Padding = new GrapeCity.ActiveReports.PaddingEx(1, 1, 2, 1);
            this.txt单价1.Style = "text-align: right; vertical-align: middle";
            this.txt单价1.Text = "txt单价1";
            this.txt单价1.Top = 0F;
            this.txt单价1.Width = 0.8700001F;
            // 
            // txt订购量1
            // 
            this.txt订购量1.DataField = "订购量";
            this.txt订购量1.Height = 0.3F;
            this.txt订购量1.Left = 3.37F;
            this.txt订购量1.Name = "txt订购量1";
            this.txt订购量1.Padding = new GrapeCity.ActiveReports.PaddingEx(1, 1, 2, 1);
            this.txt订购量1.Style = "text-align: right; vertical-align: middle";
            this.txt订购量1.Text = "txt订购量1";
            this.txt订购量1.Top = 0F;
            this.txt订购量1.Width = 0.8F;
            // 
            // txt库存量1
            // 
            this.txt库存量1.DataField = "库存量";
            this.txt库存量1.Height = 0.3F;
            this.txt库存量1.Left = 4.17F;
            this.txt库存量1.Name = "txt库存量1";
            this.txt库存量1.Padding = new GrapeCity.ActiveReports.PaddingEx(1, 1, 2, 1);
            this.txt库存量1.Style = "text-align: right; vertical-align: middle";
            this.txt库存量1.Text = "txt库存量1";
            this.txt库存量1.Top = 0F;
            this.txt库存量1.Width = 0.8F;
            // 
            // txt再订购量1
            // 
            this.txt再订购量1.DataField = "再订购量";
            this.txt再订购量1.Height = 0.3F;
            this.txt再订购量1.Left = 4.97F;
            this.txt再订购量1.Name = "txt再订购量1";
            this.txt再订购量1.Padding = new GrapeCity.ActiveReports.PaddingEx(1, 1, 2, 1);
            this.txt再订购量1.Style = "text-align: right; vertical-align: middle";
            this.txt再订购量1.Text = "txt再订购量1";
            this.txt再订购量1.Top = 0F;
            this.txt再订购量1.Width = 0.8F;
            // 
            // txt中止1
            // 
            this.txt中止1.DataField = "中止";
            this.txt中止1.Height = 0.3F;
            this.txt中止1.Left = 5.77F;
            this.txt中止1.Name = "txt中止1";
            this.txt中止1.Padding = new GrapeCity.ActiveReports.PaddingEx(1, 1, 1, 1);
            this.txt中止1.Style = "text-align: right; vertical-align: middle";
            this.txt中止1.Text = "txt中止1";
            this.txt中止1.Top = 0F;
            this.txt中止1.Width = 0.5F;
            // 
            // textBox1
            // 
            this.textBox1.DataField = "=单价 * 订购量";
            this.textBox1.Height = 0.3F;
            this.textBox1.Left = 6.27F;
            this.textBox1.Name = "textBox1";
            this.textBox1.OutputFormat = resources.GetString("textBox1.OutputFormat");
            this.textBox1.Padding = new GrapeCity.ActiveReports.PaddingEx(1, 1, 2, 1);
            this.textBox1.Style = "text-align: right; vertical-align: middle";
            this.textBox1.Text = "textBox1";
            this.textBox1.Top = 0F;
            this.textBox1.Width = 1F;
            // 
            // pageHeader
            // 
            this.pageHeader.CanGrow = false;
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.label1});
            this.pageHeader.Height = 0.90625F;
            this.pageHeader.Name = "pageHeader";
            // 
            // label1
            // 
            this.label1.CharacterSpacing = 5F;
            this.label1.ClassName = "Heading1";
            this.label1.Height = 0.783F;
            this.label1.HyperLink = null;
            this.label1.Left = 0F;
            this.label1.LineSpacing = 1F;
            this.label1.Name = "label1";
            this.label1.Style = "text-align: center; vertical-align: middle";
            this.label1.Text = "商品信息分类统计表";
            this.label1.Top = 0F;
            this.label1.Width = 7.27F;
            // 
            // pageFooter
            // 
            this.pageFooter.CanGrow = false;
            this.pageFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.reportInfo1});
            this.pageFooter.Height = 0.3F;
            this.pageFooter.Name = "pageFooter";
            // 
            // reportInfo1
            // 
            this.reportInfo1.ClassName = "Heading3";
            this.reportInfo1.FormatString = "第 {PageNumber} 页，共 {PageCount} 页";
            this.reportInfo1.Height = 0.3F;
            this.reportInfo1.Left = 3.98F;
            this.reportInfo1.Name = "reportInfo1";
            this.reportInfo1.Style = "font-family: Microsoft YaHei; font-size: 10pt; font-weight: normal; text-align: r" +
    "ight; vertical-align: middle; ddo-char-set: 1";
            this.reportInfo1.Top = 0F;
            this.reportInfo1.Width = 3.29F;
            // 
            // groupHeader1
            // 
            this.groupHeader1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(163)))), ((int)(((byte)(196)))));
            this.groupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txt类别名称1,
            this.label2,
            this.label3,
            this.label4,
            this.label8,
            this.label9,
            this.label10,
            this.label11,
            this.label12});
            this.groupHeader1.DataField = "类别名称";
            this.groupHeader1.GroupKeepTogether = GrapeCity.ActiveReports.SectionReportModel.GroupKeepTogether.FirstDetail;
            this.groupHeader1.Height = 0.5000001F;
            this.groupHeader1.Name = "groupHeader1";
            this.groupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
            // 
            // txt类别名称1
            // 
            this.txt类别名称1.DataField = "=类别ID + \"、  \" + 类别名称 + \" (包括：\" + 类别说明 + \"）\"";
            this.txt类别名称1.Height = 0.3F;
            this.txt类别名称1.Left = 0F;
            this.txt类别名称1.Name = "txt类别名称1";
            this.txt类别名称1.Padding = new GrapeCity.ActiveReports.PaddingEx(2, 2, 2, 2);
            this.txt类别名称1.Style = "background-color: #39A3C4; color: White; font-size: 12pt; font-weight: bold; text" +
    "-align: left; vertical-align: middle; ddo-char-set: 1";
            this.txt类别名称1.Text = "txt类别名称1";
            this.txt类别名称1.Top = 0F;
            this.txt类别名称1.Width = 7.27F;
            // 
            // label2
            // 
            this.label2.ClassName = "列头-10-加粗-背景";
            this.label2.Height = 0.2F;
            this.label2.HyperLink = null;
            this.label2.Left = 0F;
            this.label2.Name = "label2";
            this.label2.Style = "background-color: DarkGray; color: White";
            this.label2.Text = "产品名称";
            this.label2.Top = 0.3F;
            this.label2.Width = 1.3F;
            // 
            // label3
            // 
            this.label3.ClassName = "列头-10-加粗-背景";
            this.label3.Height = 0.2F;
            this.label3.HyperLink = null;
            this.label3.Left = 1.3F;
            this.label3.Name = "label3";
            this.label3.Style = "background-color: DarkGray; color: White";
            this.label3.Text = "单位数量";
            this.label3.Top = 0.3F;
            this.label3.Width = 1.2F;
            // 
            // label4
            // 
            this.label4.ClassName = "列头-10-加粗-背景";
            this.label4.Height = 0.2F;
            this.label4.HyperLink = null;
            this.label4.Left = 2.5F;
            this.label4.Name = "label4";
            this.label4.Style = "background-color: DarkGray; color: White";
            this.label4.Text = "单价";
            this.label4.Top = 0.3F;
            this.label4.Width = 0.8700001F;
            // 
            // label8
            // 
            this.label8.ClassName = "列头-10-加粗-背景";
            this.label8.Height = 0.2F;
            this.label8.HyperLink = null;
            this.label8.Left = 3.370001F;
            this.label8.Name = "label8";
            this.label8.Style = "background-color: DarkGray; color: White";
            this.label8.Text = "订购量";
            this.label8.Top = 0.3F;
            this.label8.Width = 0.8F;
            // 
            // label9
            // 
            this.label9.ClassName = "列头-10-加粗-背景";
            this.label9.Height = 0.2F;
            this.label9.HyperLink = null;
            this.label9.Left = 4.170001F;
            this.label9.Name = "label9";
            this.label9.Style = "background-color: DarkGray; color: White";
            this.label9.Text = "库存量";
            this.label9.Top = 0.3F;
            this.label9.Width = 0.8F;
            // 
            // label10
            // 
            this.label10.ClassName = "列头-10-加粗-背景";
            this.label10.Height = 0.2F;
            this.label10.HyperLink = null;
            this.label10.Left = 4.970001F;
            this.label10.Name = "label10";
            this.label10.Style = "background-color: DarkGray; color: White";
            this.label10.Text = "再订购量";
            this.label10.Top = 0.3F;
            this.label10.Width = 0.8F;
            // 
            // label11
            // 
            this.label11.ClassName = "列头-10-加粗-背景";
            this.label11.Height = 0.2F;
            this.label11.HyperLink = null;
            this.label11.Left = 5.77F;
            this.label11.Name = "label11";
            this.label11.Style = "background-color: DarkGray; color: White";
            this.label11.Text = "停售";
            this.label11.Top = 0.3F;
            this.label11.Width = 0.5000003F;
            // 
            // label12
            // 
            this.label12.ClassName = "列头-10-加粗-背景";
            this.label12.Height = 0.2F;
            this.label12.HyperLink = null;
            this.label12.Left = 6.27F;
            this.label12.Name = "label12";
            this.label12.Style = "background-color: DarkGray; color: White";
            this.label12.Text = "销售额";
            this.label12.Top = 0.3F;
            this.label12.Width = 1F;
            // 
            // groupFooter1
            // 
            this.groupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.label6,
            this.txt产品ID1,
            this.label7,
            this.txt单价2,
            this.textBox2,
            this.label13,
            this.label14,
            this.textBox3,
            this.label15,
            this.textBox4,
            this.line1});
            this.groupFooter1.Height = 1.08F;
            this.groupFooter1.KeepTogether = true;
            this.groupFooter1.Name = "groupFooter1";
            this.groupFooter1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
            this.groupFooter1.Format += new System.EventHandler(this.groupFooter1_Format);
            // 
            // label6
            // 
            this.label6.Height = 0.2F;
            this.label6.HyperLink = null;
            this.label6.Left = 4.97F;
            this.label6.Name = "label6";
            this.label6.Style = "font-weight: bold; text-align: right; ddo-char-set: 1";
            this.label6.Text = "产品总量：";
            this.label6.Top = 0.08000001F;
            this.label6.Width = 1.3F;
            // 
            // txt产品ID1
            // 
            this.txt产品ID1.DataField = "产品ID";
            this.txt产品ID1.Height = 0.2F;
            this.txt产品ID1.Left = 6.27F;
            this.txt产品ID1.Name = "txt产品ID1";
            this.txt产品ID1.OutputFormat = resources.GetString("txt产品ID1.OutputFormat");
            this.txt产品ID1.Style = "text-align: right";
            this.txt产品ID1.SummaryFunc = GrapeCity.ActiveReports.SectionReportModel.SummaryFunc.Count;
            this.txt产品ID1.SummaryGroup = "groupHeader1";
            this.txt产品ID1.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txt产品ID1.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txt产品ID1.Text = "txt产品ID1";
            this.txt产品ID1.Top = 0.08000001F;
            this.txt产品ID1.Width = 1F;
            // 
            // label7
            // 
            this.label7.Height = 0.2F;
            this.label7.HyperLink = null;
            this.label7.Left = 4.97F;
            this.label7.Name = "label7";
            this.label7.Style = "font-weight: bold; text-align: right; ddo-char-set: 1";
            this.label7.Text = "订购总量：";
            this.label7.Top = 0.28F;
            this.label7.Width = 1.3F;
            // 
            // txt单价2
            // 
            this.txt单价2.DataField = "订购量";
            this.txt单价2.Height = 0.2F;
            this.txt单价2.Left = 6.27F;
            this.txt单价2.Name = "txt单价2";
            this.txt单价2.OutputFormat = resources.GetString("txt单价2.OutputFormat");
            this.txt单价2.Style = "text-align: right";
            this.txt单价2.SummaryGroup = "groupHeader1";
            this.txt单价2.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txt单价2.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txt单价2.Text = "tx订购量2";
            this.txt单价2.Top = 0.28F;
            this.txt单价2.Width = 1F;
            // 
            // textBox2
            // 
            this.textBox2.DataField = "库存量";
            this.textBox2.Height = 0.2F;
            this.textBox2.Left = 6.27F;
            this.textBox2.Name = "textBox2";
            this.textBox2.OutputFormat = resources.GetString("textBox2.OutputFormat");
            this.textBox2.Style = "text-align: right";
            this.textBox2.SummaryGroup = "groupHeader1";
            this.textBox2.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox2.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox2.Text = "txt库总量2";
            this.textBox2.Top = 0.4800001F;
            this.textBox2.Width = 0.9999997F;
            // 
            // label13
            // 
            this.label13.Height = 0.2F;
            this.label13.HyperLink = null;
            this.label13.Left = 4.97F;
            this.label13.Name = "label13";
            this.label13.Style = "font-weight: bold; text-align: right; ddo-char-set: 1";
            this.label13.Text = "库存总量：";
            this.label13.Top = 0.4800001F;
            this.label13.Width = 1.3F;
            // 
            // label14
            // 
            this.label14.Height = 0.2F;
            this.label14.HyperLink = null;
            this.label14.Left = 4.97F;
            this.label14.Name = "label14";
            this.label14.Style = "font-weight: bold; text-align: right; ddo-char-set: 1";
            this.label14.Text = "再订购总量：";
            this.label14.Top = 0.6800001F;
            this.label14.Width = 1.3F;
            // 
            // textBox3
            // 
            this.textBox3.DataField = "再订购量";
            this.textBox3.Height = 0.2F;
            this.textBox3.Left = 6.27F;
            this.textBox3.Name = "textBox3";
            this.textBox3.OutputFormat = resources.GetString("textBox3.OutputFormat");
            this.textBox3.Style = "text-align: right";
            this.textBox3.SummaryGroup = "groupHeader1";
            this.textBox3.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.textBox3.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.textBox3.Text = "txt再订购量2";
            this.textBox3.Top = 0.6800001F;
            this.textBox3.Width = 0.9999997F;
            // 
            // label15
            // 
            this.label15.Height = 0.2F;
            this.label15.HyperLink = null;
            this.label15.Left = 4.969999F;
            this.label15.Name = "label15";
            this.label15.Style = "font-weight: bold; text-align: right; ddo-char-set: 1";
            this.label15.Text = "销售总金额：";
            this.label15.Top = 0.8800001F;
            this.label15.Width = 1.3F;
            // 
            // textBox4
            // 
            this.textBox4.Height = 0.2F;
            this.textBox4.Left = 6.27F;
            this.textBox4.Name = "textBox4";
            this.textBox4.OutputFormat = resources.GetString("textBox4.OutputFormat");
            this.textBox4.Style = "text-align: right";
            this.textBox4.Text = "txt销售总金额1";
            this.textBox4.Top = 0.8800001F;
            this.textBox4.Width = 0.9999997F;
            // 
            // line1
            // 
            this.line1.Height = 1.490116E-08F;
            this.line1.Left = 4.97F;
            this.line1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(163)))), ((int)(((byte)(196)))));
            this.line1.LineWeight = 1F;
            this.line1.Name = "line1";
            this.line1.Top = 0.08F;
            this.line1.Width = 2.3F;
            this.line1.X1 = 4.97F;
            this.line1.X2 = 7.27F;
            this.line1.Y1 = 0.08000001F;
            this.line1.Y2 = 0.08F;
            // 
            // rptProductsByCategory
            // 
            oleDBDataSource1.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=D:\\ActiveReports Demo\\V8\\ActiveRepor" +
    "ts for ASP.NET\\Data\\NWind_CHS.mdb;Persist Security Info=False";
            oleDBDataSource1.SQL = "SELECT 产品.*,类别.类别名称,类别.说明 AS 类别说明,类别.图片 AS 类别图片, 供应商.公司名称 AS 供应商,供应商.联系人姓名,供应商.城市" +
    ",供应商.地址\r\nFROM (供应商 INNER JOIN 产品 ON 供应商.供应商ID = 产品.供应商ID) INNER JOIN 类别 ON 产品.类别" +
    "ID = 类别.类别ID\r\nORDER BY 产品.类别ID;";
            this.DataSource = oleDBDataSource1;
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 7.270001F;
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.groupHeader1);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.groupFooter1);
            this.Sections.Add(this.pageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; color: Black; fon" +
            "t-family: \"Microsoft YaHei\"; font-size: 10pt; text-align: center; ddo-char-set: " +
            "162", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-weight: bold; font-family: \"Microsoft YaHei\"; font-size: 20pt; ddo-char-set:" +
            " 162", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-family: \"Microsoft YaHei\"; ddo-char-set: 162; font-style: i" +
            "nherit; font-weight: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-weight: bold; font-family: \"Microsoft YaHei\"; ddo-char-set: 162; font-size: " +
            "12pt", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; color: Black; fon" +
            "t-family: \"Microsoft YaHei\"; font-size: 10pt; ddo-char-set: 162; background-colo" +
            "r: Gainsboro", "Normal1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; color: Black; fon" +
            "t-family: \"Microsoft YaHei\"; font-size: 10pt; ddo-char-set: 162", "Normal2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "列头-加粗-背景", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; color: Black; font-family: \"Microsoft " +
            "YaHei\"; font-size: 10pt; text-align: center; ddo-char-set: 162; font-weight: bol" +
            "d; background-color: RosyBrown", "列头-10-加粗-背景", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; color: Black; fon" +
            "t-family: \"Microsoft YaHei\"; font-size: 10pt; text-align: center; ddo-char-set: " +
            "162; background-color: #FFC0C0", "行间隔色", "Normal"));
            this.ReportStart += new System.EventHandler(this.rptProductsByCategory_ReportStart);
            ((System.ComponentModel.ISupportInitialize)(this.picture1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt产品名称1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt单位数量1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt单价1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt订购量1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt库存量1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt再订购量1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt中止1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportInfo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt类别名称1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt产品ID1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt单价2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.Picture picture1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt产品名称1;
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;
        private GrapeCity.ActiveReports.SectionReportModel.ReportInfo reportInfo1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader groupHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter groupFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt单位数量1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt单价1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt订购量1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt库存量1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt再订购量1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt中止1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;
        private GrapeCity.ActiveReports.SectionReportModel.Label label3;
        private GrapeCity.ActiveReports.SectionReportModel.Label label4;
        private GrapeCity.ActiveReports.SectionReportModel.Label label8;
        private GrapeCity.ActiveReports.SectionReportModel.Label label9;
        private GrapeCity.ActiveReports.SectionReportModel.Label label10;
        private GrapeCity.ActiveReports.SectionReportModel.Label label11;
        private GrapeCity.ActiveReports.SectionReportModel.Label label12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt类别名称1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt产品ID1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt单价2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
        private GrapeCity.ActiveReports.SectionReportModel.Label label13;
        private GrapeCity.ActiveReports.SectionReportModel.Label label14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox3;
        private GrapeCity.ActiveReports.SectionReportModel.Label label15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox4;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
    }
}
