﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ActiveReportsHome.Master" AutoEventWireup="true" CodeBehind="Library.aspx.cs" Inherits="ActiveReportsDemo.DefaultLib" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContainer" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContainer" runat="server">
    <div class="page-content" style="min-height: 824px !important">
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN PAGE TITLE -->
                    <h3 class="page-title">欢迎查看 ActiveReports 行业报表库
                    </h3>
                    <!-- END PAGE TITLE -->
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <div id="dashboard">
                <ul class="reportlib">
                    <li class="red"><a href="Viewers/default.aspx?category=1006&action=13&t=lib">生产制造</a></li>
                    <li class="gray"><a href="Viewers/default.aspx?category=1001&action=1&t=lib">仓储物流</a></li>
                    <li class="blue"><a href="Viewers/default.aspx?category=1002&action=1&t=lib">电子商务</a></li>
                    <li class="yellow"><a href="Viewers/default.aspx?category=1003&action=1&t=lib">工商司会计</a></li>
                    <li class="orange"><a href="Viewers/default.aspx?category=1004&action=20&t=lib">煤炭资源</a></li>
                    <li class="green"><a href="Viewers/default.aspx?category=1005&action=1&t=lib">其他行业</a></li>
                </ul>
            </div>
            <style>
                .reportlib {
                    list-style: none;
                    margin: 0px 0px 0px 5px;
                }

                    .reportlib li {
                        float: left;
                        width: 310px;
                        text-align: center;
                        margin: 10px;
                        height: 150px;
                        line-height: 150px;
                    }

                        .reportlib li.red {
                            background: #cb0130;
                        }

                        .reportlib li.yellow {
                            background: #e8a900;
                        }

                        .reportlib li.orange {
                            background: #f26b0a;
                        }

                        .reportlib li.gray {
                            background: #57636d;
                        }

                        .reportlib li.blue {
                            background: #155caa;
                        }

                        .reportlib li.green {
                            background: #43aaaa;
                        }

                        .reportlib li a {
                            color: #fff;
                            font-size: 1.6em;
                        }

                            .reportlib li a:hover {
                                text-decoration: none;
                                cursor: pointer;
                            }
            </style>
        </div>
    </div>
</asp:Content>
