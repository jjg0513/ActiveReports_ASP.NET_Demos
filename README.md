#ActiveReports-ASP.NET-Demos

全方位的报表解决方案 

ActiveReports是一款在全球范围内应用非常广泛的报表控件，以提供.NET报表所需的全部报表设计功能领先于同类报表控件，包括对交互式报表的强大支持、丰富的数据可视化方式、与Visual Studio的完美集成、以及对 HTML5 / WinForm / ASP.NET / ASP.NET MVC / Silverlight / WPF 和 Windows Azure 的多平台支持等。 通过ActiveReports报表控件，您除了可以创建常用的子报表、交叉报表、分组报表、分栏报表、主从报表等商业报表外，还可以创建具备数据筛选、数据排序、数据钻取、报表互链等交互能力的数据分析报表，并把数据以可视化的方式呈现出来，快速为应用程序添加强大的报表功能。 

官网地址：http://www.gcpowertools.com.cn/products/activereports_overview.htm

下载链接：http://www.gcpowertools.com.cn/products/download.aspx?pid=16

在线演示：http://demo.gcpowertools.com.cn/activereports/aspnet/controlexplorer/

中文社区：http://gcdn.gcpowertools.com.cn/


![葡萄城控件微信服务号](http://www.gcpowertools.com.cn/newimages/qrgrapecity.png "葡萄城控件微信服务号")