﻿<%@ Page Language="C#" MasterPageFile="~/ActiveReports.Master" AutoEventWireup="true" CodeBehind="AdvanceDefault.aspx.cs" Inherits="ActiveReportsDemo.Viewers.AdvanceDefault" %>

<%@ Register Assembly="GrapeCity.ActiveReports.Web.v9, Version=9.3.4300.0, Culture=neutral, PublicKeyToken=cc4967777c49a3ff" Namespace="GrapeCity.ActiveReports.Web" TagPrefix="ActiveReportsWeb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ReportContainer" runat="server">
        <div style="width: 100%; height: 650px; margin-top: 10px; margin-right: auto; margin-bottom: 10px;
        margin-left: auto;">
         <asp:Panel ID="Panel1" runat="server" Style="width: 100%; height: auto;" 
             BorderStyle="None" BorderWidth="0px">
            <h1 align="center" style="background-color: #33CCFF; font-family: 微软雅黑; font-size: xx-large; font-weight: normal; color: #FFFFFF" >客户订单</h1>
             <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="AccessDataSource1" Height="151px" Width="100%" style="margin-top: 0px">
                 <Columns>
                     <asp:BoundField DataField="订单ID" HeaderText="订单ID" SortExpression="订单ID" />
                     <asp:BoundField DataField="客户ID" HeaderText="客户ID" SortExpression="客户ID" />
                     <asp:BoundField DataField="订购日期" HeaderText="订购日期" DataFormatString="{0:d}" SortExpression="订购日期" />
                     <asp:BoundField DataField="运货商" HeaderText="运货商" SortExpression="运货商" />
                     <asp:BoundField DataField="货主地址" HeaderText="货主地址" SortExpression="货主地址" />
                     <asp:TemplateField ShowHeader="False">
                         <ItemTemplate>
                             <asp:Button ID="Button1" runat="server" HeaderText="预览" CausesValidation="false" CommandName="Select" Text="预览" OnClick="Button1_Click" />
                         </ItemTemplate>
                         <ControlStyle BackColor="#99FFCC" />
                         <ItemStyle Width="40px" Wrap="False" />
                     </asp:TemplateField>
                     <asp:TemplateField ShowHeader="False">
                         <ItemTemplate>
                             <asp:Button ID="Button2" runat="server" HeaderText="打印" CausesValidation="false" CommandName="Select" Text="打印" OnClick="Button2_Click"/>
                         </ItemTemplate>
                         <ControlStyle BackColor="#66FFCC" />
                         <ItemStyle Width="40px" Wrap="False" />
                     </asp:TemplateField>
                 </Columns>
             </asp:GridView>
             <asp:AccessDataSource ID="AccessDataSource1" runat="server" DataFile="~/Data/NWind_CHS.mdb" SelectCommand="SELECT TOP 10 订单ID, 客户ID, 订购日期, 运货商, 货主地址 FROM 订单"></asp:AccessDataSource>
        </asp:Panel>
                <ActiveReportsWeb:WebViewer ID="WebViewer1" runat="server" Width="100%"
       ViewerType="FlashViewer" Visible="False" style="margin-top: 1px">
    </ActiveReportsWeb:WebViewer>
    </div>
</asp:Content>
