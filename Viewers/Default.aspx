﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ActiveReports.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ActiveReportsDemo.Viewers.Default" %>

<%@ Register Assembly="GrapeCity.ActiveReports.Web.v9, Version=9.3.4300.0, Culture=neutral, PublicKeyToken=cc4967777c49a3ff" Namespace="GrapeCity.ActiveReports.Web" TagPrefix="ActiveReportsWeb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ReportContainer" runat="server">
    <ActiveReportsWeb:WebViewer ID="WebViewer1" runat="server" height= "650px" Width="100%">
    </ActiveReportsWeb:WebViewer>
</asp:Content>
