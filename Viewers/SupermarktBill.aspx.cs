﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrapeCity.ActiveReports;
using System.Data;
using GrapeCity.ActiveReports.SectionReportModel;

namespace ActiveReportsDemo.Viewers
{
    public partial class SupermarktBill : System.Web.UI.Page
    {
        // 当前预览的报表
        SectionReport rpt;

        // 报表运行的总高度
        public float TotalOfHeight = 0;
        private int num = 5;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private void LoadReport()
        {
            rpt = new SectionReport();
            TotalOfHeight = 0;

            // 针对不同模板，只需修改报表名称即可
            System.Xml.XmlTextReader xtr = new System.Xml.XmlTextReader(Server.MapPath("\\Reports") + "\\rptSupermarktBill.rpx");
            rpt.LoadLayout(xtr);
            xtr.Close();

            rpt.Document.Printer.PrinterName = "";

            // 计算所有区域的总高度
            foreach (Section item in rpt.Sections)
            {
                // 先计算只显示一次的区域
                if (item is ReportHeader || item is ReportFooter || item is PageHeader || item is PageFooter)
                {
                    TotalOfHeight += item.Height;
                }

                if (item is GroupHeader || item is GroupFooter || item is Detail)
                {
                    item.Format += new EventHandler(item_AfterPrint);
                }

            }

            rpt.ReportEnd += new EventHandler(rpt_ReportEnd);

            rpt.DataSource = GetData1();

            rpt.Run();

            WebViewer1.Report = rpt;
            //button2.Enabled = true;
        }

        // 累加当前区域的高度
        void item_AfterPrint(object sender, EventArgs e)
        {
            float height = (sender as Section).Height;

            TotalOfHeight += height;
        }

        // 为当前页设置正确的高度值
        void rpt_ReportEnd(object sender, EventArgs e)
        {
            // 在报表运行结束之后调整最后一页的高度
            SectionReport rpt = (sender as SectionReport);
            rpt.Document.Pages[rpt.Document.Pages.Count - 1].Height = TotalOfHeight - (rpt.Document.Pages.Count - 1) * rpt.PageSettings.PaperWidth;
        }

        // 加载模拟数据源
        private DataTable GetData1()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("商品名");
            dt.Columns.Add("单价");
            dt.Columns.Add("数量");
            dt.Columns.Add("总金额");


            Random rdm = new Random();
            for (int g1 = 1; g1 <= num; g1++)
            {

                DataRow dr = dt.NewRow();

                dr["商品名"] = "商品" + g1;

                dr["单价"] = rdm.Next(10, 30);
                dr["数量"] = rdm.Next(1, 10);
                dr["总金额"] = rdm.Next(1, 100);

                dt.Rows.Add(dr);

            }
            return dt;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                num = Convert.ToInt32(TextBox1.Text);
            }
            catch (FormatException fe)
            {
            }

            LoadReport();
        }
    }
}