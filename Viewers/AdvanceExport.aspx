﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ActiveReports.Master" AutoEventWireup="true" CodeBehind="AdvanceExport.aspx.cs" Inherits="ActiveReportsDemo.Viewers.AdvanceExport" %>

<%@ Register Assembly="GrapeCity.ActiveReports.Web.v9, Version=9.3.4300.0, Culture=neutral, PublicKeyToken=cc4967777c49a3ff" Namespace="GrapeCity.ActiveReports.Web" TagPrefix="ActiveReportsWeb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ReportContainer" runat="server">
    <ActiveReportsWeb:WebViewer ID="WebViewer1" runat="server" Height="1220px" Width="100%"
        ViewerType="FlashViewer" ReportName="">
        <FlashViewerOptions MultiPageViewColumns="1" MultiPageViewRows="1">
        </FlashViewerOptions>
    </ActiveReportsWeb:WebViewer>
</asp:Content>