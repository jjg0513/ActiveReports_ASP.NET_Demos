﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
    CodeBehind="FormatPrint.aspx.cs" Inherits="ActiveReportsDemo.Viewers.FormatPrint" %>

<%@ Register Assembly="GrapeCity.ActiveReports.Web.v8, Version=8.0.133.0, Culture=neutral, PublicKeyToken=cc4967777c49a3ff"
    Namespace="GrapeCity.ActiveReports.Web" TagPrefix="ActiveReportsWeb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script language="javascript" type="text/javascript">
        var viewer, viewerforprint;
        var pages;

        window.onload = function () {
            GrapeCity.ActiveReports.Viewer.OnLoad("<%=WebViewer1.ClientID %>", function () {
                viewer = GrapeCity.ActiveReports.Viewer.Attach("<%=WebViewer1.ClientID %>");
                viewer.setEventsHandler({
                    OnToolClick: function (e) {
                        if (e.Tool == "套打") {
                            printreport();
                            return false;
                        }
                    },
                    OnLoadProgress: function (e) {
                        if (e.State == "Completed") {
                            pages = e.PageCount;
                        }
                    }
                });
            });

            GrapeCity.ActiveReports.Viewer.OnLoad("<%=WebViewer2.ClientID %>", function () {
                viewerforprint = GrapeCity.ActiveReports.Viewer.Attach("<%=WebViewer2.ClientID %>");
            });
        }

        function printreport() {
            var orient = "None";
            var scale = "None";
            var from = 1;
            var to = pages;
            var ops = viewerforprint.CreatePrintOptions();
            ops.AdjustPaperOrientation = orient;
            ops.ScalePages = scale;
            ops.ClearPageRanges();
            ops.AddPageRange(from, to);

            viewerforprint.Print(ops);

        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <ActiveReportsWeb:WebViewer ID="WebViewer1" runat="server" Height="1220px" Width="100%"
        ViewerType="FlashViewer" ReportName="">
        <FlashViewerOptions MultiPageViewColumns="1" MultiPageViewRows="1" UseClientApi="True">
        </FlashViewerOptions>
    </ActiveReportsWeb:WebViewer>
    <ActiveReportsWeb:WebViewer ID="WebViewer2" runat="server" Height="0" Width="0" ViewerType="FlashViewer"
        ReportName="">
        <FlashViewerOptions MultiPageViewColumns="1" MultiPageViewRows="1" UseClientApi="True">
        </FlashViewerOptions>
    </ActiveReportsWeb:WebViewer>
</asp:Content>
