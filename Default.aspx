﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ActiveReportsHome.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ActiveReportsDemo.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContainer" runat="server">

    <link href="Css/StyleSheet.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContainer" runat="server">
    <div class="page-content" style="min-height: 824px !important">
        <div class="homelayout">
            <div style="float: left; width: 900px; text-align: center; margin-top: 50px;">
                <ul class="productsarea">
                    <li id="se" class="">
                        <div style="display: block" id="senormal">
                            <a href="Feature.aspx" target="_self">
                                <p></p>
                                <h1>功能展示</h1>
                                <div class="keywords">
                                    覆盖5大类报表需求<br>
                                    完整的实现步骤、视频讲解和效果预览
                                </div>
                            </a>
                        </div>
                    </li>
                    <li id="ar" class="">
                        <div style="display: block" id="arnormal">
                            <a href="Library.aspx?t=lib" target="_self">
                                <p></p>
                                <h1>行业报表</h1>
                                <div class="keywords">
                                    涉及6大行业<br>
                                    近100张报表模板供您免费使用
                                </div>
                            </a>
                        </div>
                    </li>
                    <li id="wijmo" class="">
                        <div style="display: block" id="wijmonormal">
                            <a href="About/Requirement.aspx?category=1001&action=1&t=abt" target="_self">
                                <p></p>
                                <h1>提交需求</h1>
                                <div class="keywords">
                                    找不到所需报表和资料<br>
                                    就让我们的技术工程师来协助您吧
                                </div>
                            </a>
                        </div>
                    </li>
                    <li id="sp" class="">
                        <div style="display: block" id="spnormal">
                            <p></p>
                            <h1>联系我们</h1>
                            <div class="keywords">
                                产品功能、报表需求、购买等任何问题，欢迎联系我们<br />
                                电话：400-657-6008<br />
                                邮箱：<a href="mailto:sales.xa@grapecity.com" style="display:initial;">sales.xa@grapecity.com</a>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</asp:Content>
