﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ActiveReportsHome.Master" AutoEventWireup="true" CodeBehind="Feature.aspx.cs" Inherits="ActiveReportsDemo.Feature" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContainer" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContainer" runat="server">
    <div class="page-content" style="min-height: 824px !important">
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN PAGE TITLE -->
                    <h3 class="page-title">欢迎查看 ActiveReports 报表应用系统
                    </h3>
                    <!-- END PAGE TITLE -->
                </div>
            </div>
            <div class="padder">
                <div class="Interactive-Report">
                    <a href="Viewers/default.aspx?category=1002&action=1" title="交互式报表">
                        <img src="image/交互式报表.png" alt="交互式报表" /></a>
                </div>
                <div class="Map-Report">
                    <a href="Viewers/default.aspx?category=1004&action=10" title="地图控件">
                        <img src="image/交互式地图控件.png" alt="地图控件" /></a>
                </div>
                <div class="Visualizer-Report">
                    <a href="Viewers/default.aspx?category=1004&action=1" title="数据可视化">
                        <img src="image/数据可视化.png" alt="数据可视化" /></a>
                </div>
                <div class="Common-Report">
                    <a href="Viewers/default.aspx?category=1001&action=1" title="常规报表">
                        <img src="image/常规报表.png" alt="常规报表" /></a>
                </div>
                <div class="Controls-Report">
                    <a href="Viewers/default.aspx?category=1005&action=1" title="可扩展性">
                        <img src="image/可扩展性.png" alt="可扩展性" /></a>
                </div>
                <div class="Platform-Report">
                    <a href="Viewers/default.aspx?category=1006&action=1" title="跨平台支持">
                        <img src="image/跨平台支持.png" alt="跨平台支持" /></a>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
