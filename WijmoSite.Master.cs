﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ActiveReportsDemo
{
  public partial class WijmoSite : System.Web.UI.MasterPage
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      Page.Header.DataBind();
      if (IsPostBack)
        return;

      ltrCopyrightYear.Text = DateTime.Now.Year.ToString(System.Globalization.CultureInfo.InvariantCulture);
      ltrInitScript.Text = "<script type=\"text/javascript\" src=\"" + this.ResolveClientUrl("~/explore/js/init.js") + "\"></script>";
    }
  }
}