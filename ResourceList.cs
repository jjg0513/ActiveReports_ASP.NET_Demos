﻿using GrapeCity.ActiveReports;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;

namespace ActiveReportsDemo
{
    public class ResourceList
    {
        public string Title;
        public string Description;
        public string Keywords;
        public string Introduce;
        public string More;
        public string ReportURL;
        public string ViewType;
        public string Height;
        public string ReportType = "rdlx";//test

        public ResourceList()
        {

        }

        public ResourceList(String reportID, XElement _xmlDoc)
        {
            XElement node = _xmlDoc.Elements("Resource").Where(r => r.Attribute("ReportID").Value == reportID).First();

            this.Title = node.Element("Title").Value;
            this.Description = node.Element("Description").Value;
            this.Keywords = node.Element("Keywords").Value;
            this.Introduce = node.Element("Introduce").Value;
            this.ReportURL = node.Element("ReportURL").Value;
            this.ViewType = node.Element("ViewType").Value;
            this.Height = node.Element("Height").Value;
            this.ReportType = node.Element("ReportType").Value;
            this.ViewType = node.Element("ViewType").Value;
            this.More = node.Element("More").Value;
        }

        public void LoadReport(HttpServerUtility Server, GrapeCity.ActiveReports.Web.WebViewer wv, String DBName = "NWind_CHS")
        {

            if (ReportURL == "rptMultiSeries")
            {
                Reports.rptMultiSeries rpt11 = new Reports.rptMultiSeries();
                GrapeCity.ActiveReports.Data.OleDBDataSource oldDS = (rpt11.chartControl1.DataSource as GrapeCity.ActiveReports.Data.OleDBDataSource);
                oldDS.ConnectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0}/NWind_CHS.mdb;Persist Security Info=False", Server.MapPath("~/Data"));
                rpt11.chartControl1.DataSource = oldDS;

                wv.Report = rpt11;
            }
            else if (ReportURL == "rptControls1")
            {
                SectionReport rpt = new SectionReport();
                rpt.Document.Load(Server.MapPath("~/Reports/rptControl.rpx"));
                wv.Report = rpt;
            }
            else if (ReportURL == "rptPerformance")
            {
                SectionReport rpt = new SectionReport();
                System.Xml.XmlTextReader xtr = new System.Xml.XmlTextReader(Server.MapPath("~/Reports/rptPerformance.rpx"));
                rpt.LoadLayout(xtr);
                xtr.Close(); 
                wv.Report = rpt;
            }
            else if (ReportURL == "rptCallCenter"
                || ReportURL == "RailroadChina"
                || ReportURL == "RiverChina"
                || ReportURL == "PopulationWorld"
                || ReportURL == "rptPopulationMap")
            {
                wv.Report = LoadPageReportNoData(Server);
            }
            else
            {

                if (ReportType == "rpx")
                {
                    wv.Report = LoadSectionReport(Server);
                }
                else if (ReportType == "rdlx")
                {
                    wv.Report = LoadPageReport(Server, DBName);
                }
                switch (ViewType.ToLower())
                {
                    case "htmlviewer":
                        wv.ViewerType = GrapeCity.ActiveReports.Web.ViewerType.HtmlViewer;
                        wv.HtmlExportOptions.IncludePageMargins = true;
                        break;
                    case "rawhtml":
                        wv.ViewerType = GrapeCity.ActiveReports.Web.ViewerType.RawHtml;
                        break;
                    case "acrobatreader":
                        wv.ViewerType = GrapeCity.ActiveReports.Web.ViewerType.AcrobatReader;
                        break;
                    case "flashviewer":
                        wv.FlashViewerOptions.ResourceLocale = "zh_CN";
                        wv.ViewerType = GrapeCity.ActiveReports.Web.ViewerType.FlashViewer;
                        wv.FlashViewerOptions.ShowSplitter = false;
                        break;
                    default:
                        break;
                }
            }
            //wv.Height = Unit.Pixel(Convert.ToInt32(Height));
        }

        private GrapeCity.ActiveReports.PageReport LoadPageReport(HttpServerUtility Server,string DBName)
        {
            GrapeCity.ActiveReports.PageReport report1 = null;

            report1 = new GrapeCity.ActiveReports.PageReport(new System.IO.FileInfo(Server.MapPath("~/Reports/" + ReportURL + ".rdlx")));

            report1.Report.DataSources[0].DataSourceReference = "";
            report1.Report.DataSources[0].ConnectionProperties.DataProvider = "OLEDB";
            //report1.Report.DataSources[0].ConnectionProperties.ConnectString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};", Server.MapPath("/Data/NWind_CHS.mdb"));
            report1.Report.DataSources[0].ConnectionProperties.ConnectString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};", Server.MapPath("~/Data/" + DBName + ".mdb"));
            
            return report1;
        }

        private GrapeCity.ActiveReports.PageReport LoadPageReportNoData(HttpServerUtility Server)
        {
            GrapeCity.ActiveReports.PageReport report1 = null;
            report1 = new GrapeCity.ActiveReports.PageReport(new System.IO.FileInfo(Server.MapPath("~/Reports/" + ReportURL + ".rdlx")));

            return report1;
        }

        private GrapeCity.ActiveReports.SectionReport LoadSectionReport(HttpServerUtility Server)
        {
            GrapeCity.ActiveReports.SectionReport sr = new GrapeCity.ActiveReports.SectionReport();
            System.Xml.XmlTextReader xtr = new System.Xml.XmlTextReader(Server.MapPath("~/Reports/" + ReportURL + ".rpx"));
            sr.LoadLayout(xtr);
            xtr.Close();

            GrapeCity.ActiveReports.Data.OleDBDataSource oldDS = (sr.DataSource as GrapeCity.ActiveReports.Data.OleDBDataSource);
            oldDS.ConnectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0}/NWind_CHS.mdb;Persist Security Info=False", Server.MapPath("~/Data"));

            return sr;
        }
    }
}