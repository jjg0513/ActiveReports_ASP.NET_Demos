﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace ActiveReportsDemo
{
    public static class CommonFunctions
    {
        public static void WriteLog(string category, string str,string file)
        {
            FileStream aFile = new FileStream(file, FileMode.Append);
            StreamWriter sw = new StreamWriter(aFile);
            sw.WriteLine(string.Format("{0}\t{1}",category, str));
            sw.Close();
        }
    }
}